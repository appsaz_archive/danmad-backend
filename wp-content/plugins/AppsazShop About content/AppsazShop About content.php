<?php
/*
Plugin Name: AppsazShop About content
Plugin URI: appsaz.ir
Description: a plugin to manage the text shown in about us page
Version: 1.0
Author: Asghar Golkar
Author URI:
License: A "Slug" license name e.g. GPL2
*/

!defined('ABSPATH') AND exit;


date_default_timezone_set('Asia/Tehran');

define('ag_palapal_manage_store_FILE', __FILE__);
define('ag_palapal_manage_store_PATH', plugin_dir_path(__FILE__));
define('ag_palapal_manage_store_BASE', plugin_basename(__FILE__));
define('ag_palapal_manage_store_PLUGIN_NAME', 'ag_workingTime');

require_once (ag_palapal_manage_store_PATH.'includs/Activation.php');
require_once (ag_palapal_manage_store_PATH.'includs/ag_palapal_manage_store_Settings.php');

new ag_palapal_manage_store_Settings();
