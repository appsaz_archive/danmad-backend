<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

add_filter( 'PW_Tools_tabs', 'wooi_Tools_tabs' );

function wooi_Tools_tabs( $tabs ) {

	$tabs['wooi'] = 'فاکتور ووکامرس';

	return $tabs;
}

add_filter( 'PW_Tools_settings', 'wooi_Tools_settings' );

function load_media_files() {
	wp_enqueue_media();
}

add_action( 'admin_enqueue_scripts', 'load_media_files' );

function wooi_Tools_settings( $tools ) {

	$tools['wooi'] = [
		[
			'title' => 'تنظیمات فاکتور',
			'type'  => 'title',
			'id'    => 'tools_options'
		],
		[
			'title'   => 'نام فروشنده',
			'id'      => 'PW_Options[wooi_store_name]',
			'default' => '',
			'type'    => 'text',
			'css'     => 'width: 350px;',
			'desc'    => 'نام فروشنده یا فروشگاه جهت نمایش در صفحه فاکتور را وارد کنید',
		],
		[
			'title'   => 'آدرس',
			'id'      => 'PW_Options[wooi_store_address]',
			'default' => get_option( 'woocommerce_store_address' ),
			'type'    => 'text',
			'css'     => 'width: 350px;',
			'desc'    => 'آدرس فروشگاه جهت نمایش در صفحه فاکتور را وارد کنید',
		],
		[
			'title'   => 'کد پستی',
			'id'      => 'PW_Options[wooi_store_postcode]',
			'default' => get_option( 'woocommerce_store_postcode' ),
			'css'     => 'width: 350px;direction:ltr;',
			'type'    => 'text',
			'desc'    => 'کدپستی فروشگاه جهت نمایش در صفحه فاکتور را وارد کنید',
		],
		[
			'title'   => 'کد اقتصادی',
			'id'      => 'PW_Options[wooi_store_economic_code]',
			'default' => get_option( 'woocommerce_store_economic_code' ),
			'css'     => 'width: 350px;direction:ltr;',
			'type'    => 'text',
			'desc'    => 'کداقتصادی شرکت فروشگاه جهت نمایش در صفحه فاکتور را وارد کنید',
		],
		[
			'title' => 'شماره ثبت',
			'id'    => 'PW_Options[wooi_store_registration_number]',
			'css'   => 'width: 350px;direction:ltr;',
			'type'  => 'text',
			'desc'  => 'شماره ثبت شرکت فروشگاه جهت نمایش در صفحه فاکتور را وارد کنید',

		],
		[
			'title'   => 'استان',
			'id'      => 'PW_Options[wooi_store_state]',
			'default' => wooi_state_city_name( get_option( 'woocommerce_store_state' ) ),
			'css'     => 'width: 350px;',
			'type'    => 'text',
			'desc'    => 'استان مبدا فروشگاه  جهت نمایش در صفحه فاکتور را وارد کنید',

		],
		[
			'title'   => 'شهر',
			'id'      => 'PW_Options[wooi_store_city]',
			'default' => wooi_state_city_name( get_option( 'woocommerce_store_city' ) ),
			'type'    => 'text',
			'css'     => 'width: 350px;',
			'desc'    => 'شهر مبدا فروشگاه  جهت نمایش در صفحه فاکتور را وارد کنید',
		],
		[
			'title'   => 'تلفن',
			'id'      => 'PW_Options[wooi_store_phone]',
			'default' => '',
			'css'     => 'width: 350px;direction:ltr;',
			'type'    => 'text',
			'desc'    => 'شماره تلفن جهت نمایش در صفحه فاکتور را وارد کنید',
		],
		[
			'title'   => 'توضیحات فاکتور',
			'id'      => 'PW_Options[wooi_store_description]',
			'default' => get_bloginfo( 'description' ),
			'type'    => 'textarea',
			'css'     => 'width: 350px',
			'desc'    => 'این توضیحات در انتهای فاکتور به مشتری نمایش داده می شود.',
		],
		[
			'type' => 'sectionend',
			'id'   => 'tools_options'
		],
		[
			'title' => 'گزینه های ظاهر',
			'type'  => 'title',
			'id'    => 'tools_options_design'
		],
		[
			'title'   => 'لوگوی فاکتور',
			'id'      => 'PW_Options[wooi_store_logo]',
			'type'    => 'photo',
			'button'  => 'انتخاب لوگو',
			'default' => 'no',
		],
		[
			'title'   => 'فونت فاکتور',
			'id'      => 'PW_Options[wooi_font]',
			'default' => '',
			'type'    => 'select',
			'class'   => 'wc-enhanced-select',
			'css'     => 'width: 350px;',
			'options' => array(
				'iransans'  => 'ایران سنس',
				'vazir'     => 'وزیر',
				'iranyekan' => 'یکان',
				'tahoma'    => 'Tahoma'
			)
		],
		[
			'title'   => 'رنگ فونت',
			'id'      => 'PW_Options[wooi_font_color]',
			'default' => '#000000',
			'type'    => 'text',
			'css'     => 'width: 350px;direction:ltr',
			'desc'    => 'کد رنگ فونت فاکتور را بر اساس مقادیر هگز وارد کنید. مثال<code>#000000</code>',
		],
		[
			'title'   => 'رنگ پشت زمینه',
			'id'      => 'PW_Options[wooi_bg_color]',
			'default' => '#ffffff',
			'type'    => 'text',
			'css'     => 'width: 350px;direction:ltr',
			'desc'    => 'کد رنگ پشت زمینه را بر اساس مقادیر هگز وارد کنید. مثال<code>#FFFFFF</code>',
			//'desc_tip'  => true,
		],
		[
			'type' => 'sectionend',
			'id'   => 'tools_options_design'
		],

		[
			'title' => 'اطلاعات لایسنس افزونه',
			'type'  => 'title',
			'id'    => 'tools_options_license'
		],

		[
			'title'   => 'آدرس سایت شما',
			'id'      => 'PW_Options[wooi_license_url]',
			'default' => get_bloginfo( 'url' ),
			'type'    => 'text',
			'css'     => 'width: 350px;direction:ltr',
			'desc'    => 'آدرس صحیح فروشگاه جهت فعال سازی لایسنس را وارد کنید',
		],
		[
			'title'   => 'شماره خرید (لایسنس)',
			'id'      => 'PW_Options[wooi_license_number]',
			'default' => '',
			'type'    => 'text',
			'css'     => 'width: 350px;direction:ltr',
			'desc'    => 'شماره خرید خود را می توانید از <a href="https://woocommerce.ir/my-account/orders/" target="_blank">اینجا</a> بدست بیاورید. برای مثال: <code>55600</code>',
		],

		[
			'type' => 'sectionend',
			'id'   => 'tools_options_license'
		],

	];

	return $tools;
}

add_action( 'woocommerce_admin_field_photo', 'wooi_woocommerce_admin_field_photo' );

function wooi_woocommerce_admin_field_photo( $value ) {

	$option_value = WC_Admin_Settings::get_option( $value['id'], $value['default'] );

	$defaults = [
		'button' => 'انتخاب فایل'
	];

	$value = wp_parse_args( $value, $defaults );

	$field_description = WC_Admin_Settings::get_field_description( $value );
	$description       = $field_description['description'];
	$tooltip_html      = $field_description['tooltip_html'];

	$id = md5( $value['id'] );

	$image_url = '';
	$display   = 'none';

	if ( $image_attributes = wp_get_attachment_image_src( $option_value, 'full' ) ) {

		$image_url = $image_attributes[0];
		$display   = 'inline-block';

	}

	?>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="<?php echo esc_attr( $value['id'] ); ?>"><?php echo esc_html( $value['title'] ); ?><?php echo $tooltip_html; // WPCS: XSS ok. ?></label>
        </th>
        <td class="forminp forminp-<?php echo esc_attr( sanitize_title( $value['type'] ) ); ?>">
            <img id="<?php echo $id; ?>_image" src="<?php echo $image_url; ?>"
                 style="max-width:50%; display:block; margin-bottom: 10px;"/>

            <a href="#" id="<?php echo $id; ?>_upload_button" class="button"
               style="display: <?php echo strlen( $image_url ) ? 'none' : 'inherit'; ?>">
				<?php echo esc_attr( $value['button'] ); ?>
            </a>

            <input type="hidden" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"
                   value="<?php echo esc_attr( $option_value ); ?>"/>

            <a href="#" id="<?php echo $id; ?>_remove_button" style="display: <?php echo $display; ?>">حذف
                تصویر</a>

			<?php echo esc_html( $value['suffix'] ); ?> <?php echo $description; // WPCS: XSS ok. ?>
        </td>
    </tr>

    <script>
		jQuery(function ( $ ) {

			$('body').on('click', '#<?php echo $id; ?>_upload_button', function ( e ) {
				e.preventDefault();

				let button = $(this),
					custom_uploader = wp.media({
						title: 'درج فایل',
						library: {
							type: 'image'
						},
						button: {
							text: 'انتخاب' // button label text
						},
						multiple: false // for multiple image selection set to true
					}).on('select', function () { // it also has "open" and "close" events
						let attachment = custom_uploader.state().get('selection').first().toJSON();
						$('#<?php echo $id; ?>_image').attr('src', attachment.url).next().hide().next().val(attachment.id).next().show();
					})
					.open();
			});

			$('body').on('click', '#<?php echo $id; ?>_remove_button', function () {
				$(this).hide().prev().val('').prev().show().prev().attr('src', '');
				return false;
			});

		});
    </script>
	<?php
}
