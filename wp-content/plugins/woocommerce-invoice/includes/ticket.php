<?php
/**
 * Developer : WooCommerce.ir
 * version 4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$path = WOOI_PLUGIN_DIR . 'template/ticket/1/header.php';

require apply_filters( 'wooi_ticket_header_path', $path );

$logo = '';

$attachment_id = PW()->get_options( 'wooi_store_logo' );

if ( $image_attributes = wp_get_attachment_image_src( $attachment_id, 'full' ) ) {

	$logo = $image_attributes[0];

}

$i = 0;

foreach ( $IDs as $ID ) :

	/** @var WC_Order $order */
	$order = wc_get_order( $ID );

	$city       = wooi_state_city_name( $order->get_shipping_state() );
	$city_limit = substr( $city, 0, 10 );

	$i ++;

	$path = WOOI_PLUGIN_DIR . 'template/ticket/1/body.php';

	require apply_filters( 'wooi_ticket_body_path', $path, $order );

	$per_page = apply_filters( 'wooi_ticket_per_page', 4 );

	if ( $i % $per_page == 0 ) {
		echo '<div class="content ticket">&nbsp;</div>';
	}

endforeach;

$path = WOOI_PLUGIN_DIR . 'template/ticket/1/footer.php';

require apply_filters( 'wooi_ticket_footer_path', $path );