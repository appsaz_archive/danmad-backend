<?php
/**
 * Developer : WooCommerce.ir
 * version 4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * @param $id
 *
 * @return string
 */
function wooi_state_city_name( $id ) {

	$country_state = explode( ":", $id );

	if ( count( $country_state ) == 2 ) {
		$id = $country_state[1];
	}

	if ( is_numeric( $id ) ) {
		$state_city = get_term( $id, 'state_city' );

		if ( ! is_wp_error( $state_city ) ) {
			return $state_city->name;
		}
	}

	if ( is_numeric( $id ) && method_exists( PWS(), 'get_state' ) ) {
		$state_city = PWS()::get_state( $id );

		if ( ! is_null( $state_city ) ) {
			return $state_city;
		}

		$state_city = PWS()::get_city( $id );

		if ( ! is_null( $state_city ) ) {
			return $state_city;
		}
	}

	if ( function_exists( 'PW' ) && isset( PW()->address->states[ $id ] ) ) {
		return PW()->address->states[ $id ];
	}

	return $id;
}

function wooi_price( $price ) {
	return wooi_fa( wc_price( $price, array( 'currency' => ' ' ) ) );
}

function wooi_fa( $num ) {
	return str_replace( range( 0, 9 ), array( '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹' ), $num );
}

function wooi_show_item( $bool ) {
	echo $bool ? '' : 'style="display: none;"';
}

function wooi_barcode( $id ) {
	return "http://www.barcodes4.me/barcode/c128b/{$id}.png?height=50";
}

function wooi_qr( $str ) {
	return "https://chart.apis.google.com/chart?chs=100x100&cht=qr&chld=L|0&chl=" . $str;
}