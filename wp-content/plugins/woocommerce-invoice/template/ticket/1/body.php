<?php defined( 'ABSPATH' ) || exit; ?>
<div class="content factor">
    <div class="header">
        <div class="logo"><img src="<?php echo $logo; ?>" alt=" ">&nbsp;</div>
        <div class="text">برچسب پستی</div>
        <div class="info">
            <p>شماره سفارش: <?php echo $order->get_order_number(); ?></p>
            <p><img src="<?php echo wooi_barcode( $order->get_id() ); ?>" alt=" "></p>
        </div>
    </div>
    <div class="ticket-image"></div>
    <table>
        <tr>
            <th width="50%">گیرنده</th>
            <th width="50%">فرستنده</th>
        </tr>
        <tr>
            <td></td>
            <td>
                <p><b>فرستنده: </b><?php echo PW()->get_options( 'wooi_store_name' ); ?></p>
                <p><b>نشانی: </b><?php echo PW()->get_options( 'wooi_store_state' ); ?>
                    -<?php echo PW()->get_options( 'wooi_store_city' ); ?>
                    -<?php echo PW()->get_options( 'wooi_store_address' ); ?></p>
                <p><b>کد پستی: </b><?php echo PW()->get_options( 'wooi_store_postcode' ); ?></p>
                <p><b>تلفن: </b><?php echo PW()->get_options( 'wooi_store_phone' ); ?></p>

            </td>


        </tr>
        <tr>

            <td>
				<?php if ( $order->get_formatted_shipping_address() ) { ?>

                    <p>
                        <b>گیرنده: </b><?php echo $order->get_shipping_first_name() . " " . $order->get_shipping_last_name(); ?>
                    </p>
                    <p><b>نشانی: </b><?php echo wooi_state_city_name( $order->get_shipping_state() ); ?>
                        | <?php echo wooi_state_city_name( $order->get_shipping_city() ); ?>
                        | <?php echo $order->get_shipping_address_1(); ?>
						<?php echo $order->get_shipping_address_2(); ?>
                    </p>
                    <p>
                        <b>کد پستی: </b><?php echo $order->get_shipping_postcode(); ?>
                        <b>شیوه ارسال: </b><?php echo $order->get_shipping_method(); ?>
                    </p>
                    <p>
                        <b>شماره تماس: </b><?php echo $order->get_billing_phone(); ?>
                    </p>

				<?php } else { ?>

                    <p>
                        <b>گیرنده: </b><?php echo $order->get_billing_first_name() . " " . $order->get_billing_last_name(); ?>
                    </p>
                    <p><b>نشانی: </b><?php echo wooi_state_city_name( $order->get_billing_state() ); ?>
                        | <?php echo wooi_state_city_name( $order->get_billing_city() ); ?>
                        | <?php echo $order->get_billing_address_1(); ?>
						<?php echo $order->get_billing_address_2(); ?>
                    </p>
                    <p>
                        <b>کد پستی: </b><?php echo $order->get_billing_postcode(); ?>
                        <span <?php wooi_show_item( $order->get_shipping_method() ); ?>>
                    <b>شیوه ارسال: </b><?php echo $order->get_shipping_method(); ?>
                    </span>
                    </p>
                    <p>
                        <b>شماره تماس: </b><?php echo $order->get_billing_phone(); ?>
                    </p>
				<?php } ?>

            </td>
            <td></td>
        </tr>


    </table>
    <div class="ticket-image"></div>
</div>