<?php

/**
Plugin Name: Appsaz Shop Banners
Description: Completely dynamic and clickable banners with link support.
Version: 1.0.1
Author: Reza Askari
Author URI: https://appsaz.ir
License: GPL2
*/

include 'includes/post-types.php';
include 'includes/fields.php';
include 'includes/rest-api.php';
