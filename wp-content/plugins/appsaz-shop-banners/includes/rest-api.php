<?php

add_action('rest_api_init', 'appsazshop_banner_register_route');

function appsazshop_banner_register_route()
{
    register_rest_route('banners/v1', 'all', [
        'methods' => 'GET',
        'callback' => 'appsazshop_banner_get_all'
    ]);
}

function appsazshop_banner_get_all()
{
    $posts = get_posts([
        'post_type' => 'app_banner',
        'numberposts' => -1,
    ]);

    $banners = array_map(function ($post) {
        $banner['ID'] = $post->ID;
        $banner['title'] = $post->post_title;
        $banner['type'] = 'category';
        $banner = array_merge($banner, get_fields($post->ID));
        return $banner;
    }, $posts);

    $result = [];
    foreach ($banners as $banner) {
        if ($banner['type'] === 'product_list')
            $banner['products'] = appsazshop_banner_get_products($banner['products']);

        if ($banner['type'] === 'blog')
            $banner['blog'] = get_post($banner['blog']);

        if ($banner['double'] && $banner['second']['type'] === 'product_list')
            $banner['second']['product_list'] = appsazshop_banner_get_products($banner['second']['product_list']);

        if (!is_array($result[$banner['position']]))
            $result[$banner['position']] = [];

        array_push($result[$banner['position']], $banner);
    }

    $result = apply_filters('appsazshop_banner_get_all', $result);
    return $result;

}

/**
 * Map and array of ids to a list of product objects.
 * @param array $pids
 * @return array
 */
function appsazshop_banner_get_products($pids = [])
{
    $products = wc_get_products([
        'include' => $pids
    ]);

    return array_map(function ($product) {
        $result['id'] = $product->id;
        $result['name'] = $product->name;
        $result['price'] = $product->price;
        $result['sale_price'] = $product->sale_price;
        $result['regular_price'] = $product->regular_price;
        $result['featured'] = $product->featured;
        $result['type'] = $product->type;
        $result['attributes'] = $product->attributes;
        $result['virtual'] = $product->virtual;
        $result['downloadable'] = $product->downloadable;
        $result['category_ids'] = $product->category_ids;
        $result['gallery_image_ids'] = $product->gallery_image_ids;
        $result['image_id'] = $product->image_id;
        $result['date_created'] = $product->date_created;
        $result['thumbnail'] = wp_get_attachment_image_url($product->image_id, 'thumbnail');
        $result['stock_status'] = $product->stock_status;
        return $result;
    }, $products);
}
