<?php

function cptui_register_my_cpts_app_banner()
{

    /**
     * Post Type: بنر ها.
     */

    $labels = [
        "name" => __('Banners'),
        "singular_name" => __('Banner'),
        "menu_name" => __('Banners'),
        "all_items" => __('All banners'),
        "add_new" => __('Add'),
        "add_new_item" => __('Add new banner'),
        "edit_item" => __('Edit'),
        "new_item" => __('New banner'),
        "view_item" => __('View banner'),
        "view_items" => __('View all banners'),
        "search_items" => __('Search banners'),
        "not_found" => __('Not found'),
        "not_found_in_trash" => __('Not found in trash'),
        "featured_image" => __('Featured image'),
        "set_featured_image" => __('Set featured image'),
        "remove_featured_image" => __('Remove featured image'),
        "use_featured_image" => __('Use as featured image'),
        "archives" => __('Archive'),
        "insert_into_item" => __('Insert to banner'),
        "uploaded_to_this_item" => __('insert to this banner'),
        "filter_items_list" => __('Filter'),
        "items_list_navigation" => __('Banners navigation'),
        "items_list" => __('Banners list'),
        "attributes" => __('Attributes'),
        "name_admin_bar" => __('Add banner'),
        "item_published" => __('Banner published'),
        "item_published_privately" => __('banner published privately'),
        "item_reverted_to_draft" => __('Banner saved as draft'),
        "item_scheduled" => __('Banner saved scheduled'),
        "item_updated" => __('Banner updated'),
    ];

    $args = [
        "label" => __('Banner'),
        "labels" => $labels,
        "description" => __('Manage app banners'),
        "public" => false,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => true,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => ["slug" => "app_banner", "with_front" => false],
        "query_var" => true,
        "menu_icon" => "dashicons-format-gallery",
        "supports" => ["title", "custom-fields"],
    ];

    register_post_type("app_banner", $args);
}

add_action('init', 'cptui_register_my_cpts_app_banner');
