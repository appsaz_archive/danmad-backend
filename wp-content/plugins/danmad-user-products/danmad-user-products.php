<?php

/*
Plugin Name: Danmad User Products
Description: Return a list of related products to user.
Version: 1.1.0
Author: Reza Askari
Author URI: http://appsaz.ir
License: GPL2
*/

add_action('rest_api_init', function () {
    register_rest_route('customer', 'related-products', [
        'methods' => 'GET',
        'callback' => 'get_user_related_products',
        'args' => [
            'user_id' => [
                'required' => true,
                'description' => 'User id'
            ],
            'page' => [
                'required' => false,
            ],
            'number' => [
                'required' => false,
            ]
        ]
    ]);

});

function get_user_related_products(WP_REST_Request $request)
{
    $limit = $request->has_param('number') ? $request->get_param('number') : 10;
    $page = $request->has_param('page') ? $request->get_param('page') : 1;

    $user_id = $request->get_param('user_id');

    $product_ids = get_field('related_products', 'user_' . $user_id, false);

    if (empty($product_ids)) {
        return [];
    }

    $products = wc_get_products([
        'include' => $product_ids,
        'limit' => $limit,
        'page' => $page
    ]);

    return array_map(function (WC_Product $product) {
        $result['id'] = $product->get_id();
        $result['name'] = $product->name;
        $result['price'] = $product->price;
        $result['sale_price'] = $product->sale_price;
        $result['regular_price'] = $product->regular_price;
        $result['featured'] = $product->featured;
        $result['type'] = $product->type;
        $result['attributes'] = $product->attributes;
        $result['virtual'] = $product->virtual;
        $result['downloadable'] = $product->downloadable;
        $result['category_ids'] = $product->category_ids;
        $result['gallery_image_ids'] = $product->gallery_image_ids;
        $result['image_id'] = $product->image_id;
        $result['date_created'] = $product->date_created;
        $result['thumbnail'] = wp_get_attachment_image_url($product->image_id, 'thumbnail');
        $result['stock_status'] = $product->stock_status;
        $result['stock_quantity'] = $product->get_stock_quantity();

        return $result;
    }, $products);

}

add_action('init', function () {

    if (function_exists('acf_add_local_field_group')):

        acf_add_local_field_group(array(
            'key' => 'group_5e7c74bdba5ba',
            'title' => 'User related products',
            'fields' => array(
                array(
                    'key' => 'field_5e7c74e534864',
                    'label' => 'Related products',
                    'name' => 'related_products',
                    'type' => 'relationship',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'post_type' => array(
                        0 => 'product',
                    ),
                    'taxonomy' => '',
                    'filters' => array(
                        0 => 'search',
                        1 => 'taxonomy',
                    ),
                    'elements' => array(
                        0 => 'featured_image',
                    ),
                    'min' => '',
                    'max' => '',
                    'return_format' => 'id',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'user_form',
                        'operator' => '==',
                        'value' => 'all',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'seamless',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));

    endif;

});

