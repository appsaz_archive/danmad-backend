<?php
/*
Plugin Name: appsaz mandatory update
Plugin URI:
Description: a plugin to force app users to update their app
Version: 2.1
Author: Ghonche Yaghr
Author URI: http://appsaz.ir
License: A "Slug" license name e.g. GPL2
*/


add_action('rest_api_init', function () {

    register_rest_route("appsaz", "mandatoryupdate", array('methods' => 'get', 'callback' => function () {


        $latestAndroidPublishedVersion ="3.5.2";
        $latestIosPublishedVersion = "3.5.2";
        $isUpdateMandatory = false;


            $result = [];
            $result['latestAndroidVersion'] = $latestAndroidPublishedVersion;
            $result['latestIosVersion'] = $latestIosPublishedVersion;
            $result['mustUpdate'] = $isUpdateMandatory;

            header('Content-type: application/json; charset=utf-8');
            echo json_encode($result);
            exit ();


    }));


}
);




//********************************************service*******************************
//
//checkForUpdates(): Promise<any> {
//
//    console.log("checkForMandatoryUpdates")
//        return new Promise((resolve, reject) => {
//
//        this.HttpClient.get(this.config.url + '/wp-json/appsaz/mandatoryupdate' )
//        .subscribe(data => {
//            console.log("checkForMandatoryUpdates success");
//            console.log(data);
//            resolve(data);
//        },
//                    error => {
//            console.log("checkForMandatoryUpdates error");
//            console.log(error);
//            reject(false);
//        });
//        });
//
//    }




//*******************************check*****************************
//            this.service.checkForUpdates().then(data=>{
//
//    var latestVersion : number ;
//    var currenVersion : number ;
//
//    if(this.platform.is("android")){
//        latestVersion  = data["latestAndroidVersion"];
//        currenVersion  = this.appsazSettings.appVersion;
//
//    }
//    else if(this.platform.is("ios")){
//        latestVersion  = data["latestIosVersion"];
//        currenVersion  = this.appsazSettings.appVersion;
//
//    }
//
//    if(latestVersion > currenVersion) {
//        if (data["mustUpdate"]){
//            this.mustUpdateDialog();
//        }
//        else{
//
//            this.updateAvailableDialog();
//        }
//    }
//
//
//}, error =>{
//
//});
//
//
//    mustUpdateDialog(){
//
//    this.platform.registerBackButtonAction(() => {
//    this.platform.exitApp();
//});
//
//        let alert = this.alertCtrl.create({
//            enableBackdropDismiss: false,
//            subTitle: ' لطفا نسخه اپلیکیشن خود را به روز رسانی کنید. ',
//            buttons: [
//                {
//                    text: 'به روز رسانی',
//                    handler: () => {
//                    this.market.open("packageName");
//                    this.platform.exitApp();
//                }
//                },
//                {
//                    text: ' خروج از اپلیکیشن ',
//                    handler: () => {
//                    this.platform.exitApp();
//                }
//                }
//
//            ]
//        });
//        alert.present();
//    }
//
//
//
//    updateAvailableDialog(){
//    let alert = this.alertCtrl.create({
//            subTitle: 'نسخه جدید اپلیکیشن هم اکنون قابل دریافت است.  ',
//            buttons: [
//                {
//                    text: 'به روز رسانی',
//                    handler: () => {
//                    this.market.open("packageName");
//                }
//                },
//                {
//                    text: ' بعدا ',
//                    handler: () => {
//                    this.platform.exitApp();
//                }
//                }
//            ]
//        });
//        alert.present();
//    }


