<?php
/*
Plugin Name: register with send sms
Plugin URI: https://appsaz.ir
Description: Sign up with confirmation of phone number
Version: 1.0
Author: appsaz
Author URI: https://appsaz.ir
*/
// user registration login form
//Add frontend styles for the calendar page:
    session_start();
    
     add_shortcode( 'forgetpass_form','pippin_forgetpassword_form');
    add_shortcode( 'register_form','pippin_registration_form');
    function pippin_registration_form() {
    include( plugin_dir_path( __FILE__ ) . '/form/register.php');
        }
        function pippin_forgetpassword_form() {
    include( plugin_dir_path( __FILE__ ) . '/form/forgetpassword.php');
        }
    add_action( 'wp_enqueue_scripts', ['register_plugin_with_sms','mhdi_load_plugin_css'] );
    add_action('wp_ajax_code_send',['register_plugin_with_sms','handle_code_send']);//wp_ajax-{action name}
    add_action('wp_ajax_nopriv_code_send',['register_plugin_with_sms','handle_code_send']);
    include( plugin_dir_path( __FILE__ ) . '/vendor/autoload.php');
    add_action('wp_ajax_custom_register',['register_plugin_with_sms','handle_custom_register']);//wp_ajax-{action name}
    add_action('wp_ajax_nopriv_custom_register',['register_plugin_with_sms','handle_custom_register']);
     
     add_action("wp_ajax_get_search_list_via_ajax",['register_plugin_with_sms',"mhdi_check_username"]);
add_action("wp_ajax_nopriv_get_search_list_via_ajax",['register_plugin_with_sms',"mhdi_check_username"]);
add_action("wp_ajax_get_code_list_via_ajax",['register_plugin_with_sms',"mhdi_change_password"]);
add_action("wp_ajax_nopriv_get_code_list_via_ajax",['register_plugin_with_sms',"mhdi_change_password"]);

    class register_plugin_with_sms{
    function mhdi_load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'style1', $plugin_url . '/public/css/style.css' );
    }
    function handle_code_send()
    {
        $param = isset($_REQUEST['param']) ? trim($_REQUEST['param']) : "";
       
        if ($param == "code_test") {
            $user_mobile = $_REQUEST['user_mobile'];
               $_SESSION['mobile'] = $user_mobile;
            if(!preg_match("/^09[0-9]{9}$/", $user_mobile)){
    
                    $error_mobile = "لطفا شماره موبایلتان را به درستی وارد کنید";
                    echo json_encode(array("error" => $error_mobile));
                }
            elseif (username_exists($user_mobile)) {
                $error_mobile = 'این شماره موبایل قبلا در سایت ثبت  شده است';
                echo json_encode(array("error" => $error_mobile));
            }
            else{
                echo json_encode(array("status" => 1));
            }
        }
        wp_die();
    }
    
    function handle_custom_register()
    {
        $param = isset($_REQUEST['param']) ? trim($_REQUEST['param']) : "";
        if ($param == "register_test") {
            $first_name = $_REQUEST['first_name'];
            $last_name = $_REQUEST['last_name'];
            $email=$_REQUEST['email'];
            $username = $_SESSION['mobile'];
            $password = $_REQUEST['pwd1'];
            $answer = $_REQUEST['answer'];
            $captcha = $_REQUEST['captcha'];
            if($email!=""){
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $error_email = 'آدرس ايميل نامعتبر است!';
                    echo json_encode(array("error" => $error_email));
                }
               else if(email_exists($email) ) {
                    
                    $error_email = 'اين ايميل قبلا در سايت ثبت شده است.';
                    echo json_encode(array("error" => $error_email));
                }
                else if ($answer == $captcha) {
                $userdata = array(
                    'user_login' => $username,
                    'user_pass' => $password,
                    'user_email'=>$email,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'role' => 'customer'
                );
    
                $user = wp_insert_user($userdata);
                if (is_wp_error($user)) {
                    echo json_encode(array("status" => 0));
    
                } else {
                     echo json_encode(array("status" => 1));
                    
                }
            } 
            else {
                echo json_encode(array("status" => 2));
            }
            }
            else 
            {
            if ($answer == $captcha) {
                $userdata = array(
                    'user_login' => $username,
                    'user_pass' => $password,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'role' => 'customer'
                );
    
                $user = wp_insert_user($userdata);
                if (is_wp_error($user)) {
                    echo json_encode(array("status" => 0));
    
                } 
                else {
                     echo json_encode(array("status" => 1));
                     $user_login = get_user_by('login',  $_SESSION['mobile'] );
                    if ( !is_wp_error( $user_login ) )
                    {
                        wp_clear_auth_cookie();
                        wp_set_current_user ( $user_login->ID );
                        wp_set_auth_cookie  ( $user_login->ID );
                        $redirect_to = user_admin_url();
                
            } 
                }
            }
            else {
                echo json_encode(array("status" => 2));
            
            }
        }
    }
        wp_die();
        
    }

    function mhdi_check_username(){
     $value  = $_POST['text'];
     if(!username_exists($value)){
             echo json_encode(0);
        }
        else{
            echo json_encode(1);
        } 
     die(0);
    }
    
    function mhdi_change_password(){
     $value  = $_POST['text'];
        if($value==$_POST['code_get']){
            $user_data = get_user_by('login', $_POST['user_name'] );
            $new_password = wp_generate_password(12, false);
    		wp_set_password( $new_password, $user_data->ID );
    	
    		  wp_clear_auth_cookie();
                wp_set_current_user ( $user_data->ID );
                wp_set_auth_cookie  ( $user_data->ID );
               // $redirect_to = user_admin_url();
               	echo json_encode($new_password);
      
        }
        else{
              echo json_encode(0);
        }
     
          
         
     die(0);
    }
        }
        ?>