<div class=" register-plugin">
<div class="col-md-12">
    <p class=""id="success"></p>
    <p class="" id="error-complete"></p>
    <div class="loading" style="display:none">
    <div class="loading-wrap">
        <div class="triangle1"></div>
        <div class="triangle2"></div>
        <div class="triangle3"></div>
    </div>
    </div>
    <div id="code_mobile">
        <p class="header-form">برای ثبت نام نیاز هست که شماره موبایل شما تایید شود لطفا شماره خود را در کادر زیر وارد کنید</p>
        <p class="login-username">
            <p>موبایل: </p>
            <input type="text" id="user_mobile" name="user_mobile" class="input txt_register" value="" size="20" autocomplete="off">
            <span class="error error-form" for="user_mobile" id="user_mobile_error" >لطفا شماره موبایل خود را وارد کنید.</span>
        </p>
        <p class="btn_form">
             <input type="submit" name="submitcode" id="submitcode" class="btn-register" value="دریافت کد تایید">
        </p>
    </div>
    <div id="code_user" style="display:none">
        <p class="header-form">کدی که روی موبایلتان دریافت کردید را در کادر زیر وارد کنید</p>
        <p class="login-username">
            <p>کد دریافتی: </p>
            <input type="text" id="user_code" name="user_code" class="input txt_register" value="" size="20" autocomplete="off">
            <input type="hidden" name="coderesive" value="" id="coderesive"/>
            <span class="error error-form" for="user_code" id="user_code_error">کد دریافتی را در کادر بالا وارد کنید.</span>
        </p>
        <p class="btn_form">
        <input type="submit" name="user_code" class="btn-register" id="submituser_code" value="ارسال کد تایید">
        </p>
    </div>
    <div id="contact_form" >

        <div id="form_register" style="display:none" >
            <p class="header-form">شماره موبایل شما تایید شد برای تکمیل ثبت نام فرم زیر را با دقت پر کنید</p>
            <fieldset>
                <p class="group-input">
                    <p class="first_name">نام:</p>
                    <span class="necessary-feild">*</span>
                    <input type="text" value="" name="first_name" class="input txt_register" id="first_name" />
                    <span class="error error-form" for="first_name" id="first_name_error">نام نمی تاند خالی باشد.</span>
                </p>
                <p class="group-input">
                    <p class="last_name">نام خانوادگی:</p>
                     <span class="necessary-feild">*</span>
                    <input type="text" value="" name="last_name" class="input txt_register" id="last_name" />
                    <span class="error error-form" for="last_name" id="last_name_error">نام خانوادگی  نمی تاند خالی باشد.</span>

                </p>
                <p class="group-input">
                    <p for="email">ايميل:</p>
                    <input type="text" value="" name="email" class="input txt_register" id="email" />

                </p>
                <p class="group-input">
                    <p class="password">کلمه عبور:</p>
                     <span class="necessary-feild">*</span>
                    <input type="password" value="" name="pwd1" class="input txt_register" id="pwd1" />
                    <span class="error error-form" for="pwd1" id="pwd1_error">کلمه عبور نمی تاند خالی باشد.</span>

                </p>
                <p class="group-input">
                    <p class="password">تکرار کلمه عبور:</p>
                     <span class="necessary-feild">*</span>
                    <input type="password" value="" name="pwd2" class="input txt_register" id="pwd2" />
                    <span class="error error-form" for="pwd2" id="pwd2_error">تکرار کلمه عبور نمی تاند خالی باشد.</span>
                    <span class="error error-form" for="pwd2" id="pwd21_error">
                        کلمه عبور های وارد شده یکسان نیست 
                    </span>
                </p>
               <p class="captcha">
                    <?php

                    $digit1 = mt_rand(20,40);
                    $digit2 = mt_rand(1,20);
                    if( mt_rand(0,1) === 1 ) {
                        $math = "$digit1 + $digit2";
                        $answer = $digit1 + $digit2;
                    } else {
                        $math = "$digit2 - $digit1";
                        $answer = $digit1 - $digit2;
                    }?>
                   
                    <input type="hidden" value="<?php echo $answer; ?>" name="captcha"/>
                   <span class="matn"> <?php echo $math; ?></span> =
                   <input name="answer" type="text" id="answer"/>
                     <span class="error error-form" for="answer" id="error_answer">
                         کد امنیتی را وارد نکردید!!
                         </span>
                </p>
                <p class="btn_form">
                    <button type="submit" name="btnregister" class="button btn-register" id="register-btn" >بفرست!</button>
                </p>
            </fieldset>
        </div>
    </div>
</div>
</div>
<script>
 function myRandom(start,end){
randomNumber = start + Math.floor(Math.random() * (end-start));
return randomNumber;
}
    jQuery(function(){
        jQuery('.error').hide();
        var ajaxurl="<?php echo admin_url('admin-ajax.php')?>";
        jQuery("#submitcode").click(function(){
            jQuery('.error').hide();
            var user_mobile = jQuery("input#user_mobile").val();
            if (user_mobile == "") {
                jQuery("#user_mobile_error").show();
                jQuery("input#user_mobile").focus();
                jQuery("input#user_mobile").addClass("error-input");
                return false;
            }

             else {
                var formdata = jQuery("#code_mobile  :input").serialize();
                formdata += "&action=code_send&param=code_test";
                jQuery.ajax({
                    url: ajaxurl,
                    data: formdata,
                    type: "POST",
                    beforeSend:function() {
                        jQuery(".loading").show();
                    },
                    success: function (response) {
    
                        var data = jQuery.parseJSON(response);
                        if(data.error){
                            jQuery("#error-complete").html(data.error);
                            jQuery("#error-complete").show();
                            return false;
                        }
                        else if (data.status == 1) {
                            window.codeuser=myRandom(11111,99999);
                               jQuery.ajax({
                                        url: 'https://api.kavenegar.com/v1/506F317A50637A4D715637614B4E79345A6E69724C3630474E6134463137416B373643337735436C426F773D/verify/lookup.json?receptor='+user_mobile+'&token='+window.codeuser+'&template=petroraad',                 type: "get",
                                        success: function (response) {
                                            if(response.return.status==200){
                                                jQuery("#code_mobile").hide();
                                                jQuery("#code_user").show();
                                            }
                                            else {
                                                jQuery("#error-complete").html("متاسفانه شبکه با مشکل مواجه شد چند دقیقه بعد باز امتحان کنید");
                    
                                            }
                                         },
                

                                    });
                              } 
                    },
                    complete: function() {
                        jQuery(".loading").hide();
                        }

                });
            }
        });
    });
     
    jQuery(function(){
        jQuery('.error').hide();
        var ajaxurl="<?php echo admin_url('admin-ajax.php')?>";
        jQuery("#submituser_code").click(function(){
            jQuery('.error').hide();
            var user_code = jQuery("input#user_code").val();
            if (user_code == "") {
                jQuery("#user_code_error").show();
                jQuery("input#user_code").focus();
                jQuery("input#user_code").addClass("error-input");
                return false;
            }

            else {
                if (user_code==window.codeuser) {
                            jQuery("#code_user").hide();
                            jQuery("#form_register").show();

                        } else {
                            jQuery("#error-complete").html("کد دریافتی را به درستی وارد کنید");
                            jQuery("#error-complete").show();
                            return false;
                        }
            }
        });
    });
    jQuery(function() {
        var ajaxurl="<?php echo admin_url('admin-ajax.php')?>";
        jQuery('.error').hide();
        jQuery("#register-btn").on('click',function() {
            jQuery('.error').hide();
            var first_name = jQuery("input#first_name").val();
            if (first_name == "") {
                jQuery("#first_name_error").show();
                jQuery("input#first_name").focus();
                 jQuery("input#first_name").addClass("error-input");
                return false;
            }
            var last_name = jQuery("input#last_name").val();
            if (last_name == "") {
                jQuery("#last_name_error").show();
                jQuery("input#last_name").focus();
                 jQuery("input#last_name").addClass("error-input");
                return false;
            }
          
            var pwd1 = jQuery("input#pwd1").val();
            if (pwd1 == "") {
                jQuery("#pwd1_error").show();
                jQuery("input#pwd1").focus();
                 jQuery("input#pwd1").addClass("error-input");
                return false;
            }
            var pwd2 = jQuery("input#pwd2").val();
            if (pwd2 == "") {
                jQuery("#pwd2_error").show();
                jQuery("input#pwd2").focus();
                 jQuery("input#pwd2").addClass("error-input");
                return false;
            }
            if(pwd2!=pwd1){
                jQuery("#pwd21_error").show();
                jQuery("input#pwd2").focus();
                 jQuery("input#pwd2").addClass("error-input");
                return false;
            }
             var answer = jQuery("input#answer").val();
             if(answer==""){
                jQuery("#error_answer").show();
                jQuery("input#answer").focus();
                 jQuery("input#answer").addClass("error-input");
                return false;
            }
            else{
                var formdata=jQuery("#form_register :input").serialize();
                formdata +="&action=custom_register&param=register_test";
                jQuery.ajax({
                    url:ajaxurl,
                    type:"POST",
                    data:formdata,
                    beforeSend:function() {
                        jQuery(".loading").show();
                    },
                    success:function (response) {
                        var data=jQuery.parseJSON(response);
                       if(data.error){
                             jQuery("#error-complete").html(data.error);
                            jQuery("#error-complete").show();
                            return false;
                        }
                        if(data.status==2){
                            jQuery("#error-complete").html("کد امنیتی را به درستی وارد کنید");
                            jQuery("#error-complete").show();
                            return false;
                        }
                        else if(data.status==0){
                            jQuery("#error-complete").html("خطایی در ثبت نام ایجاد شده است بعدا امتحان کنید");
                            jQuery("#error-complete").show();
                            return false;
                        }
                        else if(data.status==1){
                            jQuery("#success").html("ثبت نام شما با موفقیت تمام شد ");
                            jQuery("#success").show();
                           window.location.href = "<?php echo user_admin_url()?>";
                          
                        }

                    },
                    complete: function() {
                        jQuery(".loading").hide();
                    }

                })
            }
        });
    });
</script>