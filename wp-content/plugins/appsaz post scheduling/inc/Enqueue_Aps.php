<?php
/**
 * Created by PhpStorm.
 * User: mhdighsmi
 * Date: 3/28/2020
 * Time: 7:45 PM
 */

namespace Inc;


class Enqueue_Aps
{
    function __construct()
    {
      //  add_action( 'wp_enqueue_scripts', array($this,'aps_enqueue_styles') );
        add_action( 'admin_enqueue_scripts', array($this,'aps_admin_enqueue_styles') );
    }
    function aps_admin_enqueue_styles() {
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'instant',APS_URL . '/public/js/admin.js');
        wp_enqueue_style( 'tare-admin', APS_URL . '/public/css/style-aps-admin.css');
    }
    public function aps_enqueue_styles() {
        wp_enqueue_script( 'instant', APS_URL . '/public//js/instant.js');
    }

}