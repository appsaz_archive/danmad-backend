jQuery(document).ready(function(){
    var days=[];
    days[0]='Saturday';
    days[1]='Monday';
    days[2]='Tuesday';
    days[3]='Wednesday';
    days[4]='Thursday';
    days[5]='Friday';
    days[6]='Sunday';
    var day='';
    var  maxField=6;
    var addButton = jQuery('.add_day_post'); //Add button selector
    var parent_data_id1 = jQuery('.add_day_post').parent().parent().attr('data-id'); //Add button selector
    var parent_id = jQuery('.add_day_post').parent().attr('id'); //Add button selector
    parent_id ++;
    var wrapper = jQuery('.field_wrapper_day'); //Input field wrapper
    var fieldHTML = '<div id="'+parent_id+'">' +
        '<input type="text" name="woo_post_scheduling['+parent_data_id1+'][day_period][]" value=""/>' +
        '<input type="number" name="woo_post_scheduling['+parent_data_id1+'][post_count][]" value=""/>' +
        '<a href="javascript:void(0);" class="add_day_post" title="Add field">'+
       ' <span class="ab-icon">+</span></a>'+
        '<a href="javascript:void(0);" class="remove_day_post"> <span class="ab-icon">-</span></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    jQuery(addButton).click(function(){
        //Check maximum number of input fields

            jQuery(this).parent('div').parent('div').append(fieldHTML); //Add field html
            jQuery(this).remove();

    });
//Once remove button is clicked
    jQuery(".field_wrapper").on('click', '.add_day_post', function(e){
        e.preventDefault();
            var parent_id = jQuery(this).parent().attr('id'); //Add button selector
            var parent_data_id1 = jQuery(this).parent().parent().attr('data-id'); //Add button selector
            parent_id ++;
            var fieldHTML = '<div id="'+parent_id+'">' +
                '<input type="text" name="woo_post_scheduling['+parent_data_id1+'][day_period][]" value=""/>' +
                '<input type="number" name="woo_post_scheduling['+parent_data_id1+'][post_count][]" value=""/>' +
                '<a href="javascript:void(0);" class="add_day_post" title="Add field">'+
                ' <span class="ab-icon">+</span></a>'+
                '<a href="javascript:void(0);" class="remove_day_post"> <span class="ab-icon">-</span></a></div>';
        console.log(fieldHTML);
            jQuery(this).parent().parent().append(fieldHTML); //Add field html
            jQuery(this).remove();
    });
    //Once remove button is clicked
    jQuery(wrapper).on('click', '.remove_day_post', function(e){
        e.preventDefault();
        jQuery(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });

    /////////addd new day
    var addButton_day = jQuery('.add_day'); //Add button selector
    var parent_data_id = jQuery('.add_day').parent().parent().attr('data-id'); //Add button selector
    parent_data_id ++;
    var wrapper_day = jQuery('.field_wrapper'); //Input field wrapper

    for(var j=0;j<days.length;j++){
        if(parent_data_id==j){
            day= days[j];
        }
    }
    var fieldHTML_day = ' <div class="field_wrapper_day" data-id="'+parent_data_id+'">'+
            ' <span class="day-count">'+day+'</span>'+
        '<div id="0">' +
        '<input type="text" name="woo_post_scheduling['+parent_data_id+'][day_period][]" value=""/>' +
        '<input type="number" name="woo_post_scheduling['+parent_data_id+'][post_count][]" value=""/>' +
        '<a href="javascript:void(0);" class="add_day_post" title="Add field">'+
        '<span class="ab-icon">+</span></a>'+
        '<a href="javascript:void(0);" class="remove_day_post"> <span class="ab-icon">-</span></a></div>'+
        '<div class="btn-add-remove"><a href="javascript:void(0);" class="add_day" title="Add field">'+
        '<span class="ab-icon">+</span></a>'+
       '<a href="javascript:void(0);" class="remove_day"><span class="ab-icon">-</span></a></div></div>';
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    jQuery(addButton_day).click(function(){
        //Check maximum number of input fields
        var parent_data_id = jQuery(this).parent().parent().attr('data-id'); //Add button selector
        if(parent_data_id < maxField) {
            jQuery(wrapper_day).append(fieldHTML_day); //Add field html
            jQuery(this).remove();
        }
    });
//Once remove button is clicked
    jQuery(wrapper_day).on('click', '.add_day', function(e){
        e.preventDefault();
        var parent_data_id = jQuery(this).parent().parent().attr('data-id'); //Add button selector
        if(parent_data_id < maxField){
            parent_data_id ++;
            for(var j=0;j<days.length;j++){
                if(parent_data_id==j){
                    day= days[j];
                }
            }
             fieldHTML_day = ' <div class="field_wrapper_day" data-id="'+parent_data_id+'">'+
                 ' <span class="day-count">'+day+'</span>'+
                '<div id="0">' +
                '<input type="text" name="woo_post_scheduling['+parent_data_id+'][day_period][]" value=""/>' +
                '<input type="number" name="woo_post_scheduling['+parent_data_id+'][post_count][]" value=""/>' +
                '<a href="javascript:void(0);" class="add_day_post" title="Add field">'+
                ' <span class="ab-icon">+</span></a>'+
                '<a href="javascript:void(0);" class="remove_day_post"> <span class="ab-icon">-</span></a></div>' +
                '<div class="btn-add-remove"><a href="javascript:void(0);" class="add_day" title="Add field">'+
                '<span class="ab-icon">+</span></a>'+
                ' <a href="javascript:void(0);" class="remove_day"> <span class="ab-icon">-</span></a></div></div>';
            x++; //Increment field counter
            jQuery(wrapper_day).append(fieldHTML_day); //Add field html
            jQuery(this).remove();
        }
    });
    //Once remove button is clicked
    jQuery(wrapper_day).on('click', '.remove_day', function(e){
        e.preventDefault();
        jQuery(this).parent('div').parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});