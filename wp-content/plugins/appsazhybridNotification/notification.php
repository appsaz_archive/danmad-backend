<?php
/*
Plugin Name: appsazhybrid-notification
Plugin URI: http://appsaz.ir
Description: Send push notification messages to application
Version: 2.0.2
Author: appsazhybrid developers
*/
date_default_timezone_set('Asia/Tehran');

//require( ABSPATH."wp-content/generalAppsaz/config.php");

register_activation_hook(__FILE__, 'appsazInboxInstall');

require('menuNavBar.php');
require('assets.php');

require('deviceList.php');
require('messageList.php');
require('messageDetail.php');

function appsazInboxInstall()
{

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $sql = '
        CREATE TABLE IF NOT EXISTS `appsazhybrid_devices` (
          `userId` varchar(254) NOT NULL,
          `category` varchar(63) DEFAULT \'general\',
          `deviceType` varchar(31),
          `deviceId` varchar(254),
          PRIMARY KEY (`userId`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
    ';
    dbDelta($sql);
    $sql = '
        CREATE TABLE IF NOT EXISTS `appsazhybrid_messages` (
          `messageId` int(11) NOT NULL AUTO_INCREMENT,
          `title` text NOT NULL,
          `content` text NOT NULL,
          `broadcast` int(4) DEFAULT 0,
          `createdAt` datetime NOT NULL,
          PRIMARY KEY (`messageId`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
    ';
    dbDelta($sql);
    $sql = '
        CREATE TABLE IF NOT EXISTS `appsazhybrid_user_message` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `userId` varchar(254),
          `messageId` int(11) NOT NULL,
          `category` varchar(63) DEFAULT \'general\',
          `seen` int(1) NOT NULL DEFAULT \'0\',
          `delivery` int(1) NOT NULL DEFAULT \'0\',
          PRIMARY KEY (`id`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;
    ';
    dbDelta($sql);
}

add_action('wp_ajax_nopriv_deviceRegistry2', 'device_registry2');

add_action('wp_ajax_nopriv_undeliveredNotif', 'undelivered_notif');
add_action('wp_ajax_nopriv_notifSeen', 'notif_seen');
add_action('wp_ajax_nopriv_notifDelivery', 'notif_delivery');
add_action('wp_ajax_nopriv_notifAll', 'notif_all');


add_action('rest_api_init', function () {
    register_rest_route('notif', 'notifall', array(
        'methods' => 'GET',
        'callback' => 'notif_all'
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('notif', 'notifdelivery', array(
        'methods' => 'GET',
        'callback' => 'notif_delivery'
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('notif', 'devicereg', array(
        'methods' => 'GET',
        'callback' => 'device_registry'
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('notif', 'notifseen', array(
        'methods' => 'GET',
        'callback' => 'notif_seen'
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('notif', 'get_unseen', array(
        'methods' => 'GET',
        'callback' => 'get_unseen'
    ));
});


function device_registry(WP_REST_Request $request)
{

    global $wpdb;
    $deviceType = $request['deviceType'];
    $deviceId = $request['deviceId'];
    $id = $request['id'];
    $result = [];

    $result['id'] = $id;

    $result = [];
    $query = "INSERT INTO appsazhybrid_devices ( userId,deviceType,deviceId ) VALUES (%s,%s,%s)";
    $inserted = $wpdb->query($wpdb->prepare($query, [$id, $deviceType, $deviceId]));
    if (!$inserted) {
        $query = "UPDATE appsazhybrid_devices SET deviceType=%s,deviceId=%s WHERE userId=%s";
        $wpdb->query($wpdb->prepare($query, [$deviceType, $deviceId, $id]));
    }
    $result['id'] = $id;
    $result['inserted'] = $inserted;
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit ();
}

function undelivered_notif(WP_REST_Request $request)
{
    global $wpdb;
    $userId = $request['userId'];
    $query = "Select * FROM appsazhybrid_user_message INNER JOIN appsazhybrid_messages ON appsazhybrid_user_message.messageId=appsazhybrid_messages.messageId  WHERE appsazhybrid_user_message.userId=%s AND appsazhybrid_user_message.delivery=%d";
    $result = $wpdb->get_results($wpdb->prepare($query, [$userId, 0]));
    $query = "UPDATE appsazhybrid_user_message SET delivery=1 WHERE userId=%s AND delivery=%d";
    $wpdb->query($wpdb->prepare($query, [$userId, 0]));
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit ();
}


function notif_all(WP_REST_Request $request)
{
    global $wpdb;
    $userId = $request['userId'];
    $offset = $request['offset'];
    $limit = $request['limit'];

    $query = "Select * FROM appsazhybrid_user_message INNER JOIN appsazhybrid_messages ON appsazhybrid_user_message.messageId=appsazhybrid_messages.messageId  WHERE appsazhybrid_user_message.userId=%s ORDER BY appsazhybrid_messages.createdAt DESC LIMIT %d, %d";
    $result = $wpdb->get_results($wpdb->prepare($query, [$userId, $offset, $limit]));
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit ();
}

function get_unseen(WP_REST_Request $request)
{
    global $wpdb;
    $userId = $request['userId'];
    $offset = $request['offset'];
    $limit = $request['limit'];

    $query = "Select * FROM appsazhybrid_user_message INNER JOIN appsazhybrid_messages ON appsazhybrid_user_message.messageId=appsazhybrid_messages.messageId  WHERE appsazhybrid_user_message.userId=%s AND appsazhybrid_user_message.seen=0  ORDER BY appsazhybrid_messages.createdAt DESC LIMIT %d, %d";
    $result = $wpdb->get_results($wpdb->prepare($query, [$userId, $offset, $limit]));

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit ();
}

function notif_seen(WP_REST_Request $request)
{
    global $wpdb;
    $messageId = $request['messageId'];
    $userId = $request['userId'];
    $result = [];
    $query = "UPDATE appsazhybrid_user_message SET seen=1 WHERE messageId=%d AND userId=%s";
    $wpdb->query($wpdb->prepare($query, [$messageId, $userId]));
    $result['id'] = $messageId;
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit ();
}

function notif_delivery(WP_REST_Request $request)
{
    global $wpdb;
    $messageId = $request['messageId'];
    $userId = $request['userId'];
    $result = [];
    $query = "UPDATE appsazhybrid_user_message SET delivery=1 WHERE messageId=%d AND userId=%s";
    $wpdb->query($wpdb->prepare($query, [$messageId, $userId]));
    $result['id'] = $messageId;
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit ();
}

add_action('admin_action_appsaz_broadcast_message', 'broadcast_message_action');


function appsazInboxAdminPage()
{

    //---------------appsaz config parameters--------------
    $largeImgNotification = false;
    //-------------appsaz config parameters end------------
    ?>


    <div class="wrap">
        <?php
        if (isset($_GET['success']))
            echo '<div class="notice  notice-success"><h1>' . __('Message sent successfully!') . '</h1></div>';
        ?>
        <form enctype='multipart/form-data' action="<?php echo admin_url('admin.php') ?>" method="post"
              onsubmit="return sendMessage()">
            <input type="hidden" name="action" value="appsaz_broadcast_message">
            <h1><?php _e('Send message') ?></h1>
            <div id="titlediv">
                <div id="titlewrap">
                    <div style="font-weight: bold; border-bottom: 1px solid #ddd; margin: 1em 0; padding: 1em 0"><?php _e('Title') ?>
                    </div>
                    <input name="title" size="30" value="" id="title" spellcheck="true" autocomplete="off"
                           type="text">
                </div>
            </div>

            <div>
                <div style="font-weight: bold; border-bottom: 1px solid #ddd; margin: 1em 0; padding: 1em 0"><?php _e('Content') ?></div>
                <div id="poststuff">
                    <?php wp_editor('', 'content'); ?>
                </div>
            </div>
            <?php
            if ($largeImgNotification) {
                ?>
                <div id="largeImgdiv">
                    <div id="titlewrap">
                        <div style="font-weight: bold; border-bottom: 1px solid #ddd; margin: 1em 0; padding: 1em 0"><?php _e('Title') ?>
                        </div>

                        <div><img id="large_img" src="" style="width: 300px;"></div>
                        <input type="file" OnChange="changeImage(event)" accept="image/*" id="large_image"
                               name="large_image">
                        <input type="hidden" name="remove_image" id="remove_image" value=0>
                        <button type="button" onclick="removeImage()"
                                class="button button-primary button-large"> <?php _e('Remove image') ?></button>
                        </br>

                    </div>
                </div>
                <?php
            }
            ?>


            <div class="tags">
                <script language="JavaScript">
                    function toggle(source) {
                        var checkboxes = document.getElementsByClassName("customTag");
                        for (var i = 0, n = checkboxes.length; i < n; i++) {
                            checkboxes[i].checked = source.checked;
                        }
                    }
                </script>
                <div style="font-weight: bold; border-bottom: 1px solid #ddd; margin: 1em 0; padding: 1em 0"><?php _e('Categories') ?>
                </div>
                <div style="display: inline-block; margin-left: 1em">
                    <input type="checkbox" onchange="toggle(this)"/>
                    <label><?php _e('All') ?></label>
                </div>
                <?php
                global $wpdb;
                $categories = $wpdb->get_results("SELECT category FROM appsazhybrid_devices GROUP BY category");
                foreach ($categories as $category) {
                    echo '<div style="display: inline-block; margin-left: 1em"><input class="customTag" type="checkbox" name="categories[]" value="' . $category->category . '"/>' . $category->category . '</div>';
                }
                ?>
            </div>
            <input type="hidden" name="categoriesCount" value="<?php echo count($categories) ?>">
            <div style="margin: 1em 0">
                <input type="submit" class="button button-primary button-large btnSend"
                       style="padding:.2em 2em; height: auto" value="<?php __('Send') ?>">
                <div class="waiting" style="display: none"><?php _e('Please wait') ?></div>
            </div>

        </form>
    </div>
    <script type="application/javascript">
        var categories;

        function sendMessage() {
            tinyMCE.triggerSave();
            var flag = true;
            var title = jQuery('#title').val();
            if (title == '') {
                alert('please enter message title');
                flag = false;
            }
            var content = jQuery('#content').val();
            if (content == '') {
                alert('please enter message content');
                flag = false;
            }
            console.log(content);
            var checked = jQuery(".tags").find(":checked");
            categories = [];
            if (checked.length == 0) {
                alert('please select category');
                flag = false;
            }
            if (flag) {
                jQuery('.btnSend').hide();
                jQuery('.waiting').show();
            }
            return flag;
        }

        function changeImage(event) {
            var output = document.getElementById('large_img');
            output.src = URL.createObjectURL(event.target.files[0]);
        }

        function removeImage() {
            var output = document.getElementById('large_img');
            output.src = "";
            jQuery('#large_image').val("");
            jQuery('#remove_image').val(1);
        }
    </script>
    <?php
}

function broadcast_message_action()
{
    //---------------appsaz config parameters--------------
    $largeImgNotification = false;
    //-------------appsaz config parameters end------------

    $showLargeImage = false;
    $largeImage = '';

    if ($largeImgNotification && isset($_FILES['large_image']) && $_FILES['large_image']['size'] && $_FILES['large_image']['size'] > 0) {
        $showLargeImage = true;
        //$file_name = $_FILES ['large_image'] ['name'];
        //$file_tmp = $_FILES ['large_image'] ['tmp_name'];
        //$file_path=$wp_upload_dir['basedir'].'//'.$file_name;
        //move_uploaded_file ($file_tmp,$file_path);
        foreach ($_FILES as $file => $array) {

            $upload_overrides = array('test_form' => false);
            $attachment_id = media_handle_upload($file, -1, null, $upload_overrides);
            $attachment_url = wp_get_attachment_url($attachment_id);
            //echo $attachment_url;

            $largeImage = $attachment_url;

            //$file_path=$attachment_url;
        }
    }


    //echo $file_path;
    //print_r($file_path);

    //echo get_temp_dir();
    //exit();

    $categories = $_REQUEST['categories'];
    $broadcast = (count($categories) == $_REQUEST['categoriesCount']) ? 1 : 0;
    broadcastMessage(stripslashes($_REQUEST['title']), stripslashes($_REQUEST['content']), $_REQUEST['categories'], $broadcast, $showLargeImage, $largeImage);

    wp_redirect(admin_url('admin.php?page=appsazMessage&success=1'));
}


function broadcastMessage($title, $content, $categories, $broadcast, $showLargeImage, $largeImage)
{

    $messageId = createMessage($title, $content, $broadcast);
    foreach ($categories as $category) {
        sendMessageToCategory($title, $content, $messageId, $category, $broadcast, $showLargeImage, $largeImage);
    }
}

function createMessage($title, $content, $broadcast = 0)
{
    global $wpdb;
    $wpdb->show_errors();
    $query = "INSERT INTO appsazhybrid_messages (title,content,broadcast,createdAt) VALUES (%s,%s,%d,%s)";
    $wpdb->query($wpdb->prepare($query, [$title, $content, $broadcast, current_time('mysql')]));
    return ($wpdb->insert_id);
}

function sendMessageToCategory($title, $content, $messageId, $category, $isBroadcast = 0, $showLargeImage = false, $largeImage = '')
{
    global $wpdb;

    $query = "SELECT userId,deviceId,deviceType FROM appsazhybrid_devices WHERE category=%s";
    $stm = $wpdb->get_results($wpdb->prepare($query, $category));
    $devices = [];
    $insertQuery = "INSERT INTO appsazhybrid_user_message (userId, messageId,category) VALUES (%s,%d, %s)";
    $wpdb->query('START TRANSACTION');
    foreach ($stm as $value) {
        $wpdb->query($wpdb->prepare($insertQuery, [$value->userId, $messageId, $category]));
        $devices[] = $value->deviceId;
        if (count($devices) > 900) {
            pushFCM($devices, $title, $content, $content, $messageId, $category, $isBroadcast, $showLargeImage, $largeImage);
            $devices = [];
        }
    }
    if (count($devices)) {
        pushFCM($devices, $title, $content, $content, $messageId, $category, $isBroadcast, $showLargeImage, $largeImage);
        $devices = [];
    }
    $wpdb->query('COMMIT');
}


function sendMessageToGroupTag($title, $content, $category)
{
    global $wpdb;
    $messageId = createMessage($title, $content);
    $users = get_users(array(
        'meta_key' => 'user_group_tag',
        'meta_value' => $category,
        'compare' => 'IN',
        'fields' => 'ids'
    ));
    $devices = [];
    foreach ($users as $user) {
        $user_meta = get_user_meta($user, "user_uuid", true);
        foreach ($user_meta as $meta) {
            $devices[] = $meta;
        }
    }
    $devicesList = join("','", $devices);
    $query = "SELECT userId,deviceId,deviceType FROM appsazhybrid_devices WHERE userId IN ('" . $devicesList . "')";
    $stm = $wpdb->get_results($query);

    $insertQuery = "INSERT INTO appsazhybrid_user_message (userId, messageId, category) VALUES (%s,%d,%s)";
    $wpdb->query('START TRANSACTION');
    foreach ($stm as $value) {
        $wpdb->query($wpdb->prepare($insertQuery, [$value->userId, $messageId, $category]));
        $devices[] = $value->deviceId;
        if (count($devices) > 900) {
            pushFCM($devices, __('New message'), $title, $content, $messageId, $category);
            $devices = [];
        }
    }
    if (count($devices)) {
        pushFCM($devices, __('New message'), $title, $content, $messageId, $category);
        $devices = [];
    }
    $wpdb->query('COMMIT');
}


function sendMessageToUser($title, $content, $userId)
{
    global $wpdb;
    $messageId = createMessage($title, $content);
    $devices = [];
    $devices = get_user_meta($userId, "user_uuid", true);

    if (!is_array($devices)) {
        $devices = [$devices];
    }

    $devicesList = join("','", $devices);
    $query = "SELECT userId,deviceId,deviceType FROM appsazhybrid_devices WHERE userId IN ('" . $devicesList . "')";
    $stm = $wpdb->get_results($query);

    $insertQuery = "INSERT INTO appsazhybrid_user_message (userId, messageId, category) VALUES (%s,%d,%s)";
    $wpdb->query('START TRANSACTION');
    foreach ($stm as $value) {
        $wpdb->query($wpdb->prepare($insertQuery, [$value->userId, $messageId, __('member')]));
        $devices[] = $value->deviceId;
        if (count($devices) > 900) {
            pushFCM($devices, __('New message'), $title, $content, $messageId, __('member'));
            $devices = [];
        }
    }
    if (count($devices)) {
        pushFCM($devices, __('New message'), $title, $content, $messageId, __('member'));
        $devices = [];
    }
    $wpdb->query('COMMIT');
}

function pushFCM($to, $title, $message, $content, $messageId, $category = "", $isBroadCast = 0, $showLargeImage = false, $largeImage = '')
{

    //---------------appsaz config parameters--------------
    $googleServiceServerAPI = "AIzaSyCIfllG-DRWhcVHtVS600iRgAMRpgQBJcc";
    //---------------appsaz config parameters--------------


    //hybrid api key
    define('API_ACCESS_KEY', $googleServiceServerAPI);

    $registrationIds = $to;
    $headers = array
    (
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );


    try {

        $fields = array
        (
            'notification' => array(
                //-------------using in fcm plugin--------------
                'title' => $title,
                'body' => $message,
                //-------------using in fcm plugin--------------
                'sound' => "default",
                'badge' => "1",
                'content_available' => false,
                'click_action' => "FCM_PLUGIN_ACTIVITY",  //Must be present for Android
            ),

            'data' => array(
                //-------------using in fcm with large image plugin--------------

                //is set true if notif bar message has large image
                'show-large-image' => $showLargeImage,
                //Notif bar large image URL (optional)
                'large-image' => $largeImage,
                //Notif bar Message title
                'title' => $title,
                //Notif bar Message body
                'body' => $message,
                //-------------using in fcm with large image plugin--------------

                //Application Message title
                'message' => $message,
                //Application Message body
                'content' => $content,
                'broadCast' => $isBroadCast,
                'category' => $category,
                'msgId' => $messageId,
            ),
            'priority' => 'high', //If not set, notification won't be delivered on completely closed iOS app
            'registration_ids' => $registrationIds,
            //"restricted_package_name"=>"" //Optional. Set for application filtering
        );


        $time_start = microtime(true);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //----------debugging-------------------
        /*if($result === FALSE) {
            echo (curl_error($ch));
            die();
        }
        else{
            $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            echo ($response);
            die();
        }*/
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        curl_close($ch);


    } catch (Exception $e) {
        $e . printStackTrace();
        return null;
    }

}


