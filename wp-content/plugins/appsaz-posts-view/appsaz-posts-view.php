<?php
/*
Plugin Name: appsaz posts view
Description: add posts view to rest API
Version: 1.0.0
Author: Amir Sasani
Author URI: http://appsaz.ir
License: GPL2
*/

class AppsazPostsView
{

    protected $post_type = 'product';

    public function __construct()
    {
        add_action("wp_ajax_nopriv_ag_add_post_views", array($this, 'add_post_view'));
        add_action("wp_ajax_nopriv_ag_get_post_views", array($this, 'get_post_view'));
        add_action("wp_ajax_nopriv_ag_is_user_view_post", array($this, 'is_user_view_post'));

        add_action("wp_ajax_nopriv_ag_add_post_like", array($this, 'add_post_favorite'));
        add_action("wp_ajax_nopriv_ag_get_post_like", array($this, 'get_favorite'));
        add_action("wp_ajax_nopriv_ag_get_post_like_all", array($this, 'post_favorite_all'));


        add_action('woocommerce_rest_prepare_product_object', [$this, 'add_product_total_views_field'], 11, 3);

        add_filter("rest_{$this->post_type}_collection_params", [$this, 'add_total_views_to_woocommerce_rest_orderby'], 100, 2);
        add_filter("as_appsaz_woocommerce_products_filter_rest_api__product_object", [$this, 'add_total_views_to_as_woocommerce_rest_filter_orderby'], 100, 2);
    }


    function add_total_views_to_woocommerce_rest_orderby($args, $request)
    {
        $args['orderby']['enum'][] = 'meta_value_num';

        return $args;
    }

    function add_product_total_views_field(WP_REST_Response $response, WC_Product $product, WP_REST_Request $request)
    {
        $total_views = get_post_meta($product->get_id(), "view_total_view", true);

        $response->data["total_views"] = $total_views ? $total_views : 0;

        return $response;
    }

    function add_total_views_to_as_woocommerce_rest_filter_orderby($_out, WC_Product $product)
    {

        $total_views = get_post_meta($product->get_id(), "view_total_view", true);
        $_out['total_views'] = $total_views ? $total_views : 0;

        return $_out;
    }

    function add_post_view()
    {
        $post_id = $_REQUEST['postId'];
        $u_id = $_REQUEST['uId'];
        $postView = get_post_meta($post_id, "view_total_view", true) ? get_post_meta($post_id, "view_total_view", true) : 0;
        $userView = get_post_meta($post_id, "view_" . $u_id, true) ? get_post_meta($post_id, "view_" . $u_id, true) : "";

        if (strlen($userView) == 0) {
            update_post_meta($post_id, "view_total_view", ++$postView);
            update_post_meta($post_id, "view_" . $u_id, $u_id);
        }

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($postView);
        exit ();
    }

    function is_user_view_post()
    {
        $postView = -1;
        if (isset($_REQUEST['postId']) && $_REQUEST['postId'] != "" && isset($_REQUEST['uId']) && $_REQUEST['uId'] != "") {
            $post_id = $_REQUEST['postId'];
            $u_id = $_REQUEST['uId'];
            $userView = get_post_meta($post_id, "view_" . $u_id, true) ? get_post_meta($post_id, "view_" . $u_id, true) : "";

            if (strlen($userView) == 0) {
                $postView = 0;
            } else {
                $postView = 1;
            }
        }
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($postView);
        exit ();
    }

    function get_post_view()
    {
        $postView = -1;
        if (isset($_REQUEST['postId']) && $_REQUEST['postId'] != "") {
            $post_id = $_REQUEST['postId'];
            $postView = get_post_meta($post_id, "view_total_view", true) ? get_post_meta($post_id, "view_total_view", true) : 0;
        }

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($postView);
        exit ();
    }

    function add_post_favorite()
    {
        $post_id = $_REQUEST['postId'];
        $user_id = $_REQUEST['uId'];
        $addStatus = 0;
        $favoritesCount = get_post_meta($post_id, "favorite_total_favorite", true) ? get_post_meta($post_id, "favorite_total_favorite", true) : 0;
        $userFavorite = get_post_meta($post_id, "favorite_" . $user_id, true) ? get_post_meta($post_id, "favorite_" . $user_id, true) : "";

        if (strlen($userFavorite) == 0) {
            update_post_meta($post_id, "favorite_total_favorite", ++$favoritesCount);
            update_post_meta($post_id, "favorite_" . $user_id, $user_id);
            $addStatus = 1;
        } else {
            update_post_meta($post_id, "favorite_total_favorite", --$favoritesCount);
            update_post_meta($post_id, "favorite_" . $user_id, "");
            $addStatus = -1;
        }

        header('Content-type: application/json; charset=utf-8');
        echo json_encode(array('likeCount' => $favoritesCount, "addStatus" => $addStatus));
        //echo json_encode( [ $addStatus ] );
        exit ();
    }

    function get_favorite()
    {
        $postLike = -1;
        if (isset($_REQUEST['postId']) && $_REQUEST['postId'] != "" && isset($_REQUEST['uId']) && $_REQUEST['uId'] != "") {
            $post_id = $_REQUEST['postId'];
            $u_id = $_REQUEST['uId'];
//    $likesCount=get_post_meta( $post_id, "like_" . $post_id, true ) ? get_post_meta( $post_id, "like_" . $post_id, true ) : 0;
            $userFavorite = get_post_meta($post_id, "favorite_" . $u_id, true) ? get_post_meta($post_id, "favorite_" . $u_id, true) : "";
            if (strlen($userFavorite) == 0) {
                $postLike = 0;
            } else {
                $postLike = 1;
            }
            //	$userFavorite = get_post_meta( $post_id, "favorite_" . $user_id, true );
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($userFavorite);
            exit ();
        }
    }

    function post_favorite_all()
    {
        $postView = -1;
        if (isset($_REQUEST['postId']) && $_REQUEST['postId'] != "") {
            $post_id = $_REQUEST['postId'];
            $postView = get_post_meta($post_id, "favorite_total_favorite", true) ? get_post_meta($post_id, "favorite_total_favorite", true) : 0;
        }

        header('Content-type: application/json; charset=utf-8');
//			$rd_args = array(
//				'numberposts' => - 1,
//				'meta_key'    => 'favorite_' . $user_id,
//				'meta_value'  => $user_id,
//				'post_type'   => 'appCatalogs',
//				'fields'      => 'ids'
//			);
//
//			$favoriteList = get_posts( $rd_args );
//			header( 'Content-type: application/json; charset=utf-8' );
        echo json_encode($postView);
        exit ();
    }


}

new AppsazPostsView();
