<?php
/*
Plugin Name: appsazWooComment
Plugin URI: https://appsaz.ir/
Description: a plugin to handle product reviews in appsaz store applications
Version: 1.0
Author: Asghar Golkar
Author URI:  https://appsaz.ir/
License: A "Slug" license name e.g. GPL2
*/
add_action('rest_api_init', function () {
    register_rest_route('woocomment', 'submitComment', array(
            'methods' => 'POST',
            'callback' => function () {
                $review = $_REQUEST['review'];
                $reviewer = $_REQUEST['reviewer'];
                $reviewer_email = $_REQUEST['reviewer_email'];
                $rating = $_REQUEST['rating'];
                $user_id = $_REQUEST['user_id'];
                $statusR = $_REQUEST['status'];
                $product_id = $_REQUEST['product_id'];
                $comment_date= current_time( 'mysql', true );

                switch ( $statusR ) {
                    case 'hold':
                    case '0':
                        $status = '0';
                        break;

                    case 'approve':
                    case '1':
                        $status = '1';
                        break;

                    case 'spam':
                    case 'trash':
                    default:
                        $status = $statusR;
                        break;
                }

                $comment_id = wp_insert_comment(array(
                    'comment_post_ID' => $product_id, // <=== The product ID where the review will show up
                    'comment_author' => $reviewer,
                    'comment_author_email' =>$reviewer_email, // <== Important
                    'comment_author_url' => '',
                    'comment_content' => $review,
                    'comment_type' => 'review',
                    'comment_parent' => 0,
                    'user_id' => $user_id, // <== Important
                    'comment_author_IP' => '',
                    'comment_agent' => '',
                    'comment_date' =>$comment_date,//date('Y-m-d H:i:s'),
                    'comment_date_gmt'=>get_gmt_from_date( $comment_date ) ,
                    'comment_approved' => $status,
                    comment_meta=>array(
                        "rating"=>$rating
                    )
                ));

				if ($comment_id){
				   header('Content-type: application/json; charset=utf-8');
                    echo json_encode(array("errors" => false, "commentId" => $comment_id ));
                    exit ();
                } else {
                    header('Content-type: application/json; charset=utf-8');
                    echo json_encode(array("errors" => true, "commentId" => 0));
                    exit ();
                }
            },
        )
    );
});

add_action('rest_api_init', function () {
    register_rest_route('woocomment', 'getUserComment', array(
            'methods' => 'POST',
            'callback' => function () {
                $username = $_REQUEST['reviewer'];
                $product_id = $_REQUEST['product'];

//                 $user = get_user_by("login", $username);
//                 $user_id = $user->ID;
                $user_id = $username;
                $args = array(
                    'user_id'  => $user_id,
                    'post_id' => $product_id, // use post_id, not post_ID
                );
                $comments = get_comments( $args );

                if($comments) {
                    $result=[];
                    foreach ($comments as $comment){
                        $result[]=array(
                            'id'             => $comment->comment_ID ,
                          //  'created_at'     => WC_API_Server::format_datetime( $comment->comment_date_gmt ),
                            'review'         => $comment->comment_content,
                            'rating'         => get_comment_meta( $comment->comment_ID, 'rating', true ),
                            'reviewer_name'  => $comment->comment_author,
                            'reviewer_email' => $comment->comment_author_email,
                         //   'verified'       => wc_review_is_from_verified_owner( $comment->comment_ID ),
                            'user_id'        => $comment->user_id,
                            'name'           => ""// $firstname.' '.$lastname,
                        );
                    }
                    header('Content-type: application/json; charset=utf-8');
                    echo json_encode($result);
                    exit ();
                }
                else
                {
                    header('Content-type: application/json; charset=utf-8');
//                     echo json_encode($_REQUEST);
                    echo json_encode('nocms');
                    exit ();
                }
            },
        )
    );
});

?>
