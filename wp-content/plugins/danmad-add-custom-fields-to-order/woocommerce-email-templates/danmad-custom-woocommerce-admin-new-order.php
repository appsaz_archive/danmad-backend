<?php $subtotal = number_format((float)$order->get_total() - $order->get_total_tax() - $order->get_total_shipping() - $order->get_shipping_tax(), wc_get_price_decimals(), '.', ''); ?>

<?php
$customer_name = '';

if (!empty($order->get_billing_first_name())) {
    $customer_name .= $order->get_billing_first_name() . ' ';
}

if (!empty($order->get_billing_last_name())) {
    $customer_name .= $order->get_billing_last_name();
}

if (!empty($order->get_billing_company())) {
    $customer_name .= ' ';
    $customer_name .= '(' . $order->get_billing_company() . ')';
}

?>

<div style="padding: 5px; border: 1px solid #000000;">
    <table style="width: 100%; margin-left: auto; margin-right: auto;" cellpadding="5">
        <tbody>
        <tr>
            <td style="width: 50%; border: 1px solid #000000;border-top-left-radius: 5px;border-bottom-left-radius: 5px;"><?= get_field('appsaz_email_header', 'option') ?></td>
            <td style="border: 1px solid #000000; border-top-right-radius: 5px;border-bottom-right-radius: 5px;">
                <p style="margin: 0;"><?php printf("Følgeseddel nr: %d", $order->get_id()) ?></p>
                <p style="margin: 0;"><?php printf("Kære %s", $customer_name) ?></p>
                <p style="margin: 0;"><?php printf("Telefon: %s", $order->get_billing_phone()) ?></p>
                <p style="margin: 0;"><?= $customer_name ?></p>
                <p style="margin: 0;"><?= $order->get_billing_address_1() ?></p>
                <p style="margin: 0;"><?= $order->get_billing_address_2() ?></p>
                <p style="margin: 0;"><?php printf("Dato: %s", $order->get_date_created()->format(get_option('date_format'))) ?></p>
                <p style="margin: 0;"><?php printf("CVR-nr: %d", get_user_meta($order->get_customer_id(), 'customer_cvr', true)) ?></p>
            </td>
        </tr>
        </tbody>
    </table>

    <table style="width: 100%; margin: 5px auto;" border="1" cellpadding="5">
        <thead>
        <tr>
            <th style="text-align: left;">Varenummer</th>
            <th style="text-align: left;">Beskrivelse</th>
            <th style="text-align: left;">Antal</th>
            <th style="text-align: left;">Enhed</th>
            <th style="text-align: left;">Enheds Pris</th>
            <th style="text-align: left;">Beløb</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($order->get_items() as $item_id => $item): ?>
            <?php
            $product_id = $item->get_product_id();
            $variation_id = $item->get_variation_id();
            $product = $item->get_product();
            $name = $item->get_name();
            $quantity = $item->get_quantity();
            $total = $item->get_total();

            $item_numebr = '';
            if ($product && !is_wp_error($product) && !empty($product->get_sku())) {
                $item_numebr = $product->get_sku();
            } else if (!empty($variation_id)) {
                $item_numebr .= sprintf("T%05d", $variation_id);
            } else {
                $item_numebr .= sprintf("T%05d", $product_id);
            }

            $unit = get_post_meta($product_id, 'product_unit', true);
            ?>
            <tr>
                <td><?= $item_numebr ?></td>
                <td><?= $name ?></td>
                <td><?= $quantity ?></td>
                <td><?= !empty($unit) ? $unit : '---' ?></td>
                <td><?= wc_price($total / $quantity) ?></td>
                <td><?= wc_price($total) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


    <?php
    $taxes = $order->get_taxes();
    $total = $order->get_total();
    ?>
    <table border="1" style="margin-left: auto;" cellpadding="5">
        <tbody>
        <tr>
            <th style="text-align: left;">Subtotal</th>
            <td><?= wc_price($subtotal) ?></td>
        </tr>
        <?php foreach ($taxes as $_tax): ?>
            <tr>
                <th style="text-align: left;"><?= sprintf("%s (%d%%)", $_tax->get_label(), intval($_tax->get_rate_percent())) ?></th>
                <td><?= wc_price($_tax->get_tax_total()) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <th style="text-align: left;">Total DKK</th>
            <td><?= wc_price($total) ?></td>
        </tr>
        </tbody>
    </table>

    <p style="text-align: center;">
        <?php printf("Delivery date: %s", get_post_meta($order->get_id(), 'deliveryDate', true)) ?>
    </p>
</div>