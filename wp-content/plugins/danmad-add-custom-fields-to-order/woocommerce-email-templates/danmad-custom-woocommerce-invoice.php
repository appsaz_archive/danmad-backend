<?php

if ($order->get_status() == 'completed') {
    include "danmad-custom-woocommerce-completed.php";
} else {
    include "danmad-custom-woocommerce-not-completed.php";
}
