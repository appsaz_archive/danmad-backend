<?php
/*
Plugin Name: Danmad - add custom fields to order
Description: add custom fields to order
Version: 1.0.0
Author: Amir Sasani
Author URI: http://appsaz.ir
License: GPL2
*/


//require_once "includes/vendor/autoload.php";

class danmadAddCustomFieldsToOrder
{
    public function __construct()
    {
        add_action('add_meta_boxes', [$this, 'add_order_metaboxes']);
        add_action('save_post', [$this, 'as_save_order_meta_on_save'], 1, 2);

        // fix email sending on application
        add_action('woocommerce_order_status_changed', [$this, 'action_woocommerce_order_status_changed'], 10, 4);

        add_action('init', [$this, 'as_change_woocommerce_emails_template']);

        register_activation_hook(__FILE__, [$this, 'copy_template_files_to_heme_directory']);

        $this->create_rest_route();
    }

    function create_rest_route()
    {
        add_action('rest_api_init', function () {
            register_rest_route(
                'invoices',
                'test',
                array(
                    'methods' => WP_REST_Server::ALLMETHODS,
                    'callback' => [$this, 'copy_template_files_to_heme_directory'],
                )
            );

            register_rest_route(
                'invoices',
                'download_pdf',
                array(
                    'methods' => WP_REST_Server::READABLE,
                    'callback' => [$this, 'download_order_details_pdf'],
                )
            );
        });
    }

    function action_woocommerce_order_status_changed($this_get_id, $this_status_transition_from, $this_status_transition_to, WC_Order $order)
    {
        if ($this_status_transition_to == 'on-hold') {
            $wc_emails = WC()->mailer()->get_emails();
            $email_id = 'customer_on_hold_order';
            foreach ($wc_emails as $wc_mail) {
                if ($wc_mail->id == $email_id) {
                    $wc_mail->trigger($order->get_id());
                }
            }
        }
    }

    function copy_template_files_to_heme_directory()
    {
        $files_to_move = [
            [
                'danmad-custom-woocommerce-not-completed.php',
                'customer-on-hold-order.php'
            ],
            [
                'danmad-custom-woocommerce-not-completed.php',
                'customer-processing-order.php'
            ],
            [
                'danmad-custom-woocommerce-not-completed.php',
                'customer-refunded-order.php'
            ],


            [
                'danmad-custom-woocommerce-completed.php',
                'customer-completed-order.php'
            ],

            [
                'danmad-custom-woocommerce-invoice.php',
                'customer-invoice.php'
            ],
            [
                'danmad-custom-woocommerce-completed.php',
                'danmad-custom-woocommerce-completed.php'
            ],
            [
                'danmad-custom-woocommerce-not-completed.php',
                'danmad-custom-woocommerce-not-completed.php'
            ],
            [
                'danmad-custom-woocommerce-admin-new-order.php',
                'admin-new-order.php'
            ],
            [
                'danmad-custom-woocommerce-order-details.php',
                'danmad-custom-woocommerce-order-details.php'
            ]
        ];

        $source_dir = __DIR__ . '/woocommerce-email-templates/';
        $destination_dir = get_template_directory() . '/woocommerce/emails/';

        // create destination path if not exists
        try {
            mkdir($destination_dir, 0755, true);

            foreach ($files_to_move as $file_array) {
                list($source_file_name, $destination_file_name) = $file_array;

                copy($source_dir . $source_file_name, $destination_dir . $destination_file_name);
            }
        } catch (Exception $e) {
            error_log(__METHOD__ . " =>  Cannot create directory {$destination_dir}");
        }
    }

    function add_order_metaboxes()
    {
        add_meta_box(
            'as_order_custom_meta__user_payment_deadline',
            'user payment deadline',
            [$this, 'as_order_custom_meta__user_payment_deadline__callback'],
            'shop_order',
            'side',
            'high'
        );
    }

    function as_order_custom_meta__user_payment_deadline__callback()
    {
        global $post;

        $last_value = get_post_meta($post->ID, 'user_payment_deadline', true);

        if (empty($last_value)) {
            $last_value = 7;
        }

        $this->showTemplate('order-payment-deadline', ['last_value' => $last_value]);
    }

    function as_save_order_meta_on_save($post_id, $post)
    {
        $user_order_payment_deadline = $_POST['as_user_order_payment_deadline_input'];
        update_post_meta($post_id, 'user_payment_deadline', $user_order_payment_deadline);

        $order_hash = md5($post_id);
        update_post_meta($post_id, 'order_hash', $order_hash);
    }

    function showTemplate($tName, $args = [])
    {
        include_once("templates/as-$tName-template.php");
    }

    function download_order_details_pdf(WP_REST_Request $request)
    {
        $data = $request->get_params();
        $order_id_hash = $data['id'];

        $order_id_hash = get_post_meta($order_id_hash, 'order_hash', true);

        $query = wc_get_orders([
            'limit' => 1,
            'return' => 'ids',
            'meta_key' => 'order_hash',
            'meta_value' => $order_id_hash,
        ]);

        $order_id = current($query);


        if (!empty($order_id)) {
            $order = wc_get_order($order_id);
            if ($order && !is_wp_error($order)) {

                header("Content-Type: text/html");
                include_once "woocommerce-email-templates/danmad-custom-woocommerce-order-details.php";

            }
        }

//        return new WP_REST_Response("END :(");

    }

}

new danmadAddCustomFieldsToOrder();

if (function_exists('acf_add_options_sub_page')) {

    acf_add_options_sub_page([
        'page_title' => 'Appsaz - Custom email settings',
        'menu_title' => 'Custom email settings',
        'parent_slug' => 'woocommerce',
    ]);

}
if (function_exists('acf_add_local_field_group')):

    acf_add_local_field_group(array(
        'key' => 'group_5ea15656decc9',
        'title' => 'Appsaz Custom email settings',
        'fields' => array(
            array(
                'key' => 'field_5ea156b4601e2',
                'label' => 'Email header',
                'name' => 'appsaz_email_header',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'basic',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5ea157f3601e3',
                'label' => 'Email footer',
                'name' => 'appsaz_email_footer',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'basic',
                'media_upload' => 1,
                'delay' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-custom-email-settings',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'seamless',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;
