<?php
/**
 * @package  AppsazAdminInterface
 */
namespace Inc;
if ( ! defined( 'ABSPATH' ) ) {

    exit; // Exit if accessed directly.

}

class Deactivate
{
    public static function deactivate() {

        self::delete_page();
        self::aai_translation();
    }
    private static function delete_page()
    {
        $new_page_title = "صفحه اصلی اپ ساز";
        $page_check = get_page_by_title($new_page_title,OBJECT,'page' );
        wp_delete_post($page_check);

        $new_page_title = "ورود پنل مدیریت";
        $page_check = get_page_by_title($new_page_title,OBJECT,'page' );
        wp_delete_post(105);
    }

    private static function aai_translation()
    {

        $currentFilePath =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/languages/appsaz-admin-fa_IR.po';
        $currentFilePath1 =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/languages/appsaz-admin-fa_IR.mo';

        $newFilePath = ABSPATH . "wp-content/languages/plugins/appsaz-admin-fa_IR.po";
        $newFilePath1 = ABSPATH . "wp-content/languages/plugins/appsaz-admin-fa_IR.mo";

        $fileMoved = rename($newFilePath,$currentFilePath);
        $fileMoved = rename($newFilePath1,$currentFilePath1);


        $loginpage =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/themplate/appsaz-login.php';
        $loginbase =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/themplate/wp-login.php';

        $loginpage1 = ABSPATH .'appsaz-login.php';
        $loginbase1 = ABSPATH .'wp-login.php';

        rename($loginpage1,$loginpage);
        rename($loginbase,$loginbase1);
    }

}