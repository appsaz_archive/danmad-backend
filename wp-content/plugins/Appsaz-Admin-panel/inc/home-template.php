<!DOCTYPE html>
<html lang="en">
<?php $options=get_option('wpappsaz_options');?>
<head>
    <link rel="shortcut icon" type="image/icon" href="<?php echo $options['favicon_app'];?>"/>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap -->
    <link href="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Slick slider -->
    <link href="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/css/slick.css" rel="stylesheet">
    <!-- Theme color -->
    <link id="switcher" href="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/css/theme-color/<?php echo $options['theme_color'].'.css';?>" rel="stylesheet">


    <link href="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/css/style.css" rel="stylesheet">


    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>



<!-- Start Header -->
<?php if($options['section1']){?>
<header id="mu-header" class="" role="banner">
    <div class="mu-header-overlay">
        <div class="container">
            <div class="mu-header-area">

                <!-- Start Logo -->
                <div class="mu-logo-area">
                    <!-- text based logo -->
                   <?php  if(!$options['logo_app']){?>
                    <a class="mu-logo" href="#"><?php if(isset($options['app_name']))echo $options['app_name'];?></a>
                    <?php }
                        else
                             {;
                   ?>
                     <a class="mu-logo" href="#"><img src="<?php if(isset($options['logo_app'])) echo $options['logo_app'];?>" alt="logo img"></a>
                    <?php }?>
                </div>
                <!-- End Logo -->

                <!-- Start header featured area -->
                <div class="mu-header-featured-area">
                    <div class="mu-header-featured-img">
                        <?php  if(!$options['screenshot_main_page']){?>
                            <img src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/iphone.png" alt="iphone image">
                        <?php }else{?>
                            <img src="<?php if(isset($options['screenshot_main_page'])) echo $options['screenshot_main_page']?>" alt="iphone image">
                        <?php  } ?>
                    </div>

                    <div class="mu-header-featured-content">
                        <h1><?php echo $options['app_name'];?> &nbsp; &nbsp;<span> <?php echo __('welcome to','appsaz-admin')?></span></h1>
                        <p><?php echo $options['description_app'];?> </p>

                        <div class="mu-app-download-area">
                            <h4><?php echo __('Download The App','appsaz-admin')?></h4>
                            <a class="mu-apple-btn" href="<?php echo $options['link_download_app_store'];?>"><i class="fa fa-apple"></i><span>apple store</span></a>
                            <a class="mu-google-btn" href="<?php echo $options['link_download_google_play'];?>"><i class="fa fa-android"></i><span>google play</span></a>
                            <!-- <a class="mu-windows-btn" href="#"><i class="fa fa-windows"></i><span>windows store</span></a> -->
                        </div>

                    </div>
                </div>
                <!-- End header featured area -->

            </div>
        </div>
    </div>
</header>
<?php }?>
<!-- End Header -->

<!-- Start Menu -->
<button class="mu-menu-btn">
    <i class="fa fa-bars"></i>
</button>
<div class="mu-menu-full-overlay">
    <div class="mu-menu-full-overlay-inner">
        <a class="mu-menu-close-btn" href="#"><span class="mu-line"></span></a>
        <nav class="mu-menu" role="navigation">
            <ul>
                <?php if($options['section2']){?>
                <li><a href="#mu-feature"><?php echo __('App Feature','appsaz-admin')?></a></li>
                <?php }?>
                <?php if($options['section3']){?>
                <li><a href="#mu-video"><?php echo __('Promo Video','appsaz-admin')?></a></li>
                <?php }?>
                <?php if($options['section4']){?>
                <li><a href="#mu-apps-screenshot"><?php echo __('Apps Screenshot','appsaz-admin')?></a></li>
                <?php }?>
                <?php if($options['section5']){?>
                <li><a href="#mu-download"><?php echo __('Download','appsaz-admin')?></a></li>
                <?php }?>
                <?php if($options['section6']){?>
                <li><a href="#mu-faq"><?php echo __('FAQ','appsaz-admin')?></a></li>
                <?php }?>
                <?php if($options['section7']){?>
                <li><a href="#mu-contact"><?php echo __('Get In Touch','appsaz-admin')?></a></li>
                <?php }?>
            </ul>
        </nav>
    </div>
</div>
<!-- End Menu -->



<!-- Start main content -->
<main role="main">

    <!-- Start Feature -->
    <?php if($options['section2']){?>
    <section id="mu-feature">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-feature-area">

                        <div class="mu-title-area">
                            <h2 class="mu-title"><?php echo __('OUR APP FEATURES','appsaz-admin');?></h2>
                            <span class="mu-title-dot"></span>
                            <p><?php echo $options['all_description_app']; ?></p>
                        </div>

                        <!-- Start Feature Content -->
                        <div class="mu-feature-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mu-feature-content-right">

                                        <?php if($options['feature_title1']){?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <button class="btn mu-feature-btn" type="button">
                                                        <i class="fa fa-sliders" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                                <div class="media-body">
                                                    <h3 class="media-heading"> <?php   echo $options['feature_title1'];?></h3>
                                                    <p><?php echo $options['feature_title1_desc'];?></p>
                                                </div>
                                            </div>
                                        <?php }?>
                                        <?php if($options['feature_title2']){?>
                                        <div class="media">
                                            <div class="media-left">
                                                <button class="btn mu-feature-btn" type="button">
                                                    <i class="fa fa-sliders" aria-hidden="true"></i>
                                                </button>
                                            </div>

                                            <div class="media-body">
                                                <h3 class="media-heading"> <?php   echo $options['feature_title2'];?></h3>
                                                <p><?php echo $options['feature_title2_desc'];?></p>
                                            </div>
                                        </div>
                                        <?php }?>

                                        <?php if($options['feature_title3']){?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <button class="btn mu-feature-btn" type="button">
                                                        <i class="fa fa-sliders" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                                <div class="media-body">
                                                    <h3 class="media-heading"> <?php   echo $options['feature_title3'];?></h3>
                                                    <p><?php echo $options['feature_title3_desc'];?></p>
                                                </div>
                                            </div>
                                        <?php }?>

                                        <?php if($options['feature_title4']){?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <button class="btn mu-feature-btn" type="button">
                                                        <i class="fa fa-sliders" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                                <div class="media-body">
                                                    <h3 class="media-heading"> <?php   echo $options['feature_title4'];?></h3>
                                                    <p><?php echo $options['feature_title4_desc'];?></p>
                                                </div>
                                            </div>
                                        <?php }?>

                                    </div>
                                    <!-- End single feature item -->

                                </div>
                                <div class="col-md-6">
                                    <div class="mu-feature-content-left">
                                        <?php if(! $options['screenshot_two_page']){?>
                                            <img class="mu-profile-img" src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/iphone-group.png" alt="iphone Image">
                                        <?php } else{?>
                                            <img class="mu-profile-img" src="<?php echo $options['screenshot_two_page'];?>" alt="iphone Image">
                                        <?php }?>
                                    </div>
                                </div>

                                </div>
                            </div>
                        </div>
                        <!-- End Feature Content -->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <!-- End Feature -->

    <!-- Start Video -->
    <?php if($options['section3']){?>
    <section id="mu-video">
        <div class="mu-video-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mu-video-area">
                            <h2><?php echo __('Watch Promo Video','appsaz-admin');?></h2>
                            <a class="mu-video-play-btn" href="#"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Start Video content -->
        <div class="mu-video-content">
            <div class="mu-video-iframe-area">
                <a class="mu-video-close-btn" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
                <?php if(! $options['Watch_Promo_Video']){?>
                    <iframe class="mu-video-iframe" width="854" height="480" src="https://www.youtube.com/embed/9r40_ffCZ_I" frameborder="0" allowfullscreen></iframe>
                <?php } else {?>
                    <video class="video-fluid z-depth-1" loop controls muted>
                        <source class="mu-video-iframe" width="854" height="480" src="<?php echo $options['Watch_Promo_Video'];?>" frameborder="0" allowfullscreen  type="video/mp4" />
                    </video>

                <?php }?>
            </div>
        </div>
        <!-- End Video content -->

    </section>
    <?php }?>
    <!-- End Video -->

    <!-- Start Apps Screenshot -->
    <?php if($options['section4']){?>
    <section id="mu-apps-screenshot">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-apps-screenshot-area">

                        <div class="mu-title-area">
                            <h2 class="mu-title"><?php echo __('APPS SCREENSHOT','appsaz-admin');?></h2>
                            <span class="mu-title-dot"></span>
                            <p><?php echo $options['description_app'];?></p>
                        </div>


                        <!-- Start Apps Screenshot Content -->
                        <div class="mu-apps-screenshot-content">

                            <div class="mu-apps-screenshot-slider">

                                <div class="mu-single-screeshot">
                                    <?php if(! $options['screenshot01']){?>
                                        <img src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/screenshot/01.jpg" alt="App screenshot img">
                                    <?php } else{?>
                                        <img src="<?php echo $options['screenshot01'];?>" alt="App screenshot img">

                                    <?php }?>
                                </div>


                                <div class="mu-single-screeshot">
                                    <?php if(! $options['screenshot02']){?>
                                        <img src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/screenshot/02.jpg" alt="App screenshot img">
                                    <?php } else{?>
                                        <img src="<?php echo $options['screenshot02'];?>" alt="App screenshot img">

                                    <?php }?>
                                </div>


                                <div class="mu-single-screeshot">
                                    <?php if(! $options['screenshot03']){?>
                                        <img src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/screenshot/03.jpg" alt="App screenshot img">
                                    <?php } else{?>
                                        <img src="<?php echo $options['screenshot03'];?>" alt="App screenshot img">

                                    <?php }?>
                                </div>



                                <div class="mu-single-screeshot">
                                    <?php if(! $options['screenshot04']){?>
                                        <img src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/screenshot/04.jpg" alt="App screenshot img">
                                    <?php } else{?>
                                        <img src="<?php echo $options['screenshot04'];?>" alt="App screenshot img">

                                    <?php }?>
                                </div>



                                <div class="mu-single-screeshot">
                                    <?php if(! $options['screenshot05']){?>
                                        <img src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/screenshot/05.jpg" alt="App screenshot img">
                                    <?php } else{?>
                                        <img src="<?php echo $options['screenshot05'];?>" alt="App screenshot img">

                                    <?php }?>
                                </div>

                                <div class="mu-single-screeshot">
                                    <?php if(! $options['screenshot06']){?>
                                        <img src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/images/screenshot/04.jpg" alt="App screenshot img">
                                    <?php } else{?>
                                        <img src="<?php echo $options['screenshot06'];?>" alt="App screenshot img">

                                    <?php }?>
                                </div>

                            </div>

                        </div>
                        <!-- End Apps Screenshot Content -->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <!-- End Apps Screenshot -->

    <!-- Start Download -->
    <?php if($options['section5']){?>
    <section id="mu-download">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-download-area">

                        <div class="mu-title-area">
                            <h2 class="mu-title"><?php echo __('GET THE APP','appsaz-admin');?></h2>
                            <span class="mu-title-dot"></span>
                            <p><?php if(isset($options['description_app'])) echo $options['description_app'];?> </p>
                        </div>


                        <div class="mu-download-content">
                            <a class="mu-apple-btn" href="<?php echo $options['link_download_app_store'];?>"><i class="fa fa-apple"></i><span>apple store</span></a>
                            <a class="mu-google-btn" href="<?php echo $options['link_download_google_play'];?>"><i class="fa fa-android"></i><span>google play</span></a>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <!-- End Download -->

    <!-- Start FAQ -->
    <?php if($options['section6']){?>
    <section id="mu-faq">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-faq-area">

                        <div class="mu-title-area">
                            <h2 class="mu-title"><?php echo __('FAQ','appsaz-admin');?></h2>
                            <span class="mu-title-dot"></span>
                        </div>


                        <div class="mu-faq-content">

                            <div class="panel-group" id="accordion">
                                <?php if($options['FAQ01']){?>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                                <span class="fa fa-minus"></span><?php echo $options['FAQ01'];?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <?php echo $options['FAQ_desc01'];?>
                                        </div>
                                    </div>
                                </div>

                                <?php }?>

                                <?php if($options['FAQ02']){?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                                    <span class="fa fa-minus"></span><?php echo $options['FAQ02'];?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <?php echo $options['FAQ_desc02'];?>
                                            </div>
                                        </div>
                                    </div>

                                <?php }?>
                                <?php if($options['FAQ03']){?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                                    <span class="fa fa-minus"></span><?php echo $options['FAQ03'];?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <?php echo $options['FAQ_desc03'];?>
                                            </div>
                                        </div>
                                    </div>

                                <?php }?>
                                <?php if($options['FAQ04']){?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                                    <span class="fa fa-minus"></span><?php echo $options['FAQ04'];?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <?php echo $options['FAQ_desc04'];?>
                                            </div>
                                        </div>
                                    </div>

                                <?php }?>
                                <?php if($options['FAQ05']){?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                                    <span class="fa fa-minus"></span><?php echo $options['FAQ05'];?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <?php echo $options['FAQ_desc05'];?>
                                            </div>
                                        </div>
                                    </div>

                                <?php }?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <!-- End FAQ -->


    <!-- Start Contact -->
    <?php if($options['section7']){?>
    <section id="mu-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-contact-area">

                        <div class="mu-title-area">
                            <h2 class="mu-heading-title"><?php echo __('GET IN TOUCH','appsaz-admin');?></h2>
                            <span class="mu-title-dot"></span>
                        </div>

                        <!-- Start Contact Content -->
                        <div class="mu-contact-content">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="mu-contact-right">
                                        <?php if($options['Office_Location']){?>
                                            <div class="mu-contact-right-single">
                                                <div class="mu-icon"><i class="fa fa-map-marker"></i></div>
                                                <p><strong><?php echo __('Office Location','appsaz-admin'); ?></strong></p>
                                                <p><?php echo $options['Office_Location'];?></p>
                                            </div>
                                        <?php }?>
                                        <?php if($options['Phone_Number']){?>
                                            <div class="mu-contact-right-single">
                                                <div class="mu-icon"><i class="fa fa-phone"></i></div>
                                                <p><strong><?php echo __('Phone Number','appsaz-admin'); ?></strong></p>
                                                <p><?php echo $options['Phone_Number'];?></p>
                                            </div>
                                        <?php }?>
                                        <?php if($options['Email_Address']){?>
                                            <div class="mu-contact-right-single">
                                                <div class="mu-icon"><i class="fa fa-envelope"></i></div>
                                                <p><strong><?php echo __('Email Address','appsaz-admin'); ?></strong></p>
                                                <p><?php echo $options['Email_Address'];?></p>
                                            </div>
                                        <?php }?>

                                        <div class="mu-contact-right-single">
                                            <div class="mu-social-media">
                                                <?php if($options['whatsapp_link']){?>
                                                    <a href="https://wa.me/<?php echo $options['whatsapp_link'];?>"><i class="fa fa-whatsapp"></i></a>
                                                <?php }?>
                                                <?php if($options['telegram_link']){?>
                                                    <a href="https://t.me/<?php echo $options['telegram_link'];?>"><i class="fa fa-telegram"></i></a>
                                                <?php }?>
                                                <?php if($options['facebook_link']){?>
                                                    <a href="https://www.facebook.com/<?php echo $options['facebook_link'];?>"><i class="fa fa-facebook"></i></a>
                                                <?php }?>
                                                <?php if($options['twitter_link']){?>
                                                    <a href="https://twitter.com/<?php echo $options['twitter_link'];?>"><i class="fa fa-twitter"></i></a>
                                                <?php }?>

                                                <?php if($options['linkedin_link']){?>
                                                    <a href="https://www.linkedin.com/company/<?php echo $options['linkedin_link'];?>"><i class="fa fa-linkedin"></i></a>
                                                <?php }?>
                                                <?php if($options['youtube_link']){?>
                                                    <a href="https://www.youtube.com/c/<?php echo $options['youtube_link'];?>"><i class="fa fa-youtube"></i></a>
                                                <?php }?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="mu-contact-left" id="contact-box">
                                        <div id="form-messages"></div>
                                        <div class="loader" id="loader-contact">Loading...</div>
                                        <form id="ajax-contact" method="post" action="mailer.php" class="mu-contact-form">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="<?php echo __('Enter Name','appsaz-admin'); ?>" id="name" name="name" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="<?php echo __('Enter Phone','appsaz-admin'); ?>" id="phone" name="name" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="<?php echo __('Enter Email','appsaz-admin'); ?>" id="email" name="email" required>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="<?php echo __('Message','appsaz-admin'); ?>" id="message" name="message" required></textarea>
                                            </div>
                                            <button type="submit" id="createticket" class="mu-send-msg-btn"><span><?php echo __('SUBMIT','appsaz-admin'); ?></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Contact Content -->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <!-- End Contact -->

</main>
<!-- End main content -->


<!-- Start footer -->
<footer id="mu-footer" role="contentinfo">
    <div class="container">
        <div class="mu-footer-area-namad">
            <?php if($options['zarinpal_namd']){?>
                <?php echo $options['zarinpal_namd']?>
            <?php }?>

            <?php if($options['enamad']){?>
                <?php echo $options['enamad']?>
            <?php }?>

        </div>
        <div class="mu-footer-area">
            <p class="mu-copy-right">&copy; Copyright <a rel="nofollow" href="https://appsaz.ir">appsaz</a>. All right reserved.</p>
        </div>
    </div>

</footer>

<!-- End footer -->



<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap -->
<script src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/js/bootstrap.min.js"></script>
<!-- Slick slider -->
<script type="text/javascript" src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/js/slick.min.js"></script>
<!-- Ajax contact form  -->
<script type="text/javascript" src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/js/app.js"></script>



<!-- Custom js -->
<script type="text/javascript" src="<?php echo plugin_dir_url(dirname(__FILE__));?>/public/assets/js/custom.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#createticket").click(function(e){
            e.preventDefault();
            var name=$('#name').val();
            var email=$("#email").val();
            var phone=$("#phone").val();
            var message=$("#message").val();
            if(email !=""){
                jQuery.ajax({
                    type : "POST",
                    dataType : "json",
                    data:{
                        "name":name,
                        "email":email,
                        'phone':phone,
                        'message':message

                    },
                    url : '/wp-json/pmcontact/v1/create',
                    beforeSend:function(){
                        $("#contact-box").addClass("disabled-box");
                        $("#loader-contact").show();
                    },
                    success: function(response) {
                        data=JSON.parse(response);
                        console.log(data.status);

                        if(data.status==1){
                            swal("ثبت شد !", "پیام شما با موفقفیت ارسال شد!", "success");
                            window.location.reload();
                        }
                        else{
                            swal(" خطا !", "!", "error");
                            window.location.reload();
                        }


                    },
                    complete:function(){
                        $("#contact-box").removeClass("disabled-box");
                        $("#loader-contact").hide();
                    },
                });
            }
            else{
                swal("عنوانی برای پیام خود وارد کنید", "!", "error");
            }
        });
    });
</script>



</body>
</html>

     