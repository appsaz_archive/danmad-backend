<?php
/**
 * Created by PhpStorm.
 * User: mhdighsmi
 * Date: 11/26/2019
 * Time: 12:45 AM
 */

namespace Inc;

if ( ! defined( 'ABSPATH' ) ) {exit;}

class Contact
{

    function register(){
        add_action('init',array($this,'create_custom_type_ticket'));
        add_action( 'rest_api_init',array($this, 'wap_register_route_pm' ));
        add_action('admin_init', array($this, 'contact_information'));
        add_action('save_post', array($this, 'add_contact_fields'), 10, 2);
    }

    public function create_custom_type_ticket()
    {
        register_post_type('PmContact',
            array(
                'labels' => array(
                    'name' => __('message contact us',''),
                    'singular_name' => __('contact us',''),
                    'add_new' => __('add contact us',''),
                    'add_new_item' => __('add new contact us',''),
                    'edit' => __('edit',''),
                    'edit_item' => __('edit',''),
                    'new_item' => __('new contact us',''),
                    'view' => __('show',''),
                    'view_item' => __('show',''),
                    'search_items' =>__('search contact us',''),
                    'not_found' => __('not found contact us',''),
                    'not_found_in_trash' => __('empty',''),
                ),

                'public' => true,
                'menu_position' => 16,
                'supports' => array('title','editor'),
                'taxonomies' => array(''),
                'menu_icon' => 'dashicons-format-chat',
                'has_archive' => true
            )
        );

    }
    public function contact_information() {
        add_meta_box( 'PmContact_meta_box',
            'مشخصات فرستنده پیام',
            array($this,'display_contact_meta_box'),
            'PmContact'
        );
    }

    public function display_contact_meta_box($contact)
    {

        $user_name = esc_html(get_post_meta($contact->ID, 'emd_PmContact_name', true));
        $user_email = esc_html(get_post_meta($contact->ID, 'emd_PmContact_email', true));
        $user_phone = esc_html(get_post_meta($contact->ID, 'emd_PmContact_phone', true));
?>
        <table>
        <tr>
            <td style="width: 100%">نام و نام خانوادگی </td>
            <td><input type="text" id="name" size="80" name="PmContact_name" value="<?php echo $user_name; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 100%">ایمیل</td>
            <td><input type="text" size="80" name="PmContact_email" value="<?php echo $user_email; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 100%">شماره تلفن</td>
            <td><input type="text" size="80" name="PmContact_phone" value="<?php echo $user_phone; ?>" /></td>
        </tr>
        </table>
<?php
    }
    public function add_contact_fields( $sahamdar_id, $sahamdar )
    {
        if ($sahamdar->post_type == 'PmContact') {
            if (isset($_POST['PmContact_name'])) {
                update_post_meta($sahamdar_id, 'emd_PmContact_name', $_POST['PmContact_name']);
            }

            if (isset($_POST['PmContact_email'])) {
                update_post_meta($sahamdar_id, 'emd_PmContact_email', $_POST['PmContact_email']);
            }

            if (isset($_POST['PmContact_phone'])) {
                update_post_meta($sahamdar_id, 'emd_PmContact_phone', $_POST['PmContact_phone']);
            }
        }
    }
    function wap_register_route_pm() {
        register_rest_route(
            'pmcontact/v1',
            '/create',
            array(
                'methods' => 'POST',
                'callback' => array($this,'wpshout_create_pmcontact'),
            )
        );
    }
    function wpshout_create_pmcontact( $data ) {
        $name = $data['name'];
        $email = $data['email'];
        $phone = $data['phone'];
        $Content =$data['message'];

        $PmIp = $_SERVER['REMOTE_ADDR'];
        $defaults = array(
            'post_author' => $name,
            'post_content' => $Content,
            'post_title' =>   $email,
            'post_status' => 'publish',
            'post_type' => 'PmContact',
            'comment_status' => 'open',
        );
        $post_id = wp_insert_post($defaults);
        wp_set_object_terms($post_id, $data['category_ticket'], 'ticket_taxonomy');
        update_post_meta($post_id, 'emd_PmContact_name', $name, '');
        update_post_meta($post_id, 'emd_PmContact_email', $email, '');
        update_post_meta($post_id, 'emd_PmContact_phone', $phone, '');
        update_post_meta($post_id, 'wpas_form_submitted_ip', $PmIp, '');
        return json_encode(array(
            "status"=>1,
        ));

    }
}