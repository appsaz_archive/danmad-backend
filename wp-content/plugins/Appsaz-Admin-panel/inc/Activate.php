<?php
/**
 * @package  AppsazAdminInterface
 */

namespace Inc;

if ( ! defined( 'ABSPATH' ) ) {

    exit; // Exit if accessed directly.

}


class Activate
{
    public static function activate() {
        self::create_home_page();
        self::aai_translation();


    }
    public static function create_home_page(){

        $new_page_title = 'home-page-appsaz';
        $new_page_content = '';
        $new_page_template = 'goodtobebad-template.php'; //ex. template-custom.php. Leave blank if you don't want a custom page template.

        //don't change the code bellow, unless you know what you're doing

        $page_check = get_page_by_title($new_page_title);

        $new_page = array(
            'post_type' => 'page',
            'post_title' => $new_page_title,
            'post_content' => $new_page_content,
            'post_status' => 'publish',
            'post_author' => 1,
        );
        if(!isset($page_check->ID)) {
            $new_page_id = wp_insert_post($new_page);
            if (!empty($new_page_template)) {
                update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
            }
        }
        /////home page

        $home_page_title = 'single-page-appsaz';
        $home_page_content = '';
        $home_page_template = 'home-template.php'; //ex. template-custom.php. Leave blank if you don't want a custom page template.

        //don't change the code bellow, unless you know what you're doing

        $page_home = get_page_by_title($home_page_title);

        $home_page = array(
            'post_type' => 'page',
            'post_title' => $home_page_title,
            'post_content' => $home_page_content,
            'post_status' => 'publish',
            'post_author' => 1,
        );
        if(!isset($page_home->ID)) {
            $home_page_id = wp_insert_post($home_page);
            if (!empty($new_page_template)) {
                update_post_meta($home_page_id, '_wp_page_template', $home_page_template);
            }
        }
        return $page_home;
    }

    private static function aai_translation(){
        $currentFilePath =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/languages/appsaz-admin-fa_IR.po';
        $currentFilePath1 =ABSPATH . 'wp-content/plugins/Appsaz-Admin-panel/languages/appsaz-admin-fa_IR.mo';

        $newFilePath = ABSPATH . "wp-content/languages/plugins/appsaz-admin-fa_IR.po";
        $newFilePath1 = ABSPATH . "wp-content/languages/plugins/appsaz-admin-fa_IR.mo";

        $fileMoved = rename($currentFilePath,$newFilePath);
        $fileMoved = rename($currentFilePath1,$newFilePath1);
    }

}