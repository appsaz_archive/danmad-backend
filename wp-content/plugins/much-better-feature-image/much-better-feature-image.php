<?php

/**
 * Plugin Name:       Much better feature image
 * Description:       Much much better feature image :)
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Reza Askari
 * Author URI:        https://rezareza.ir/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

add_filter( 'rest_post_dispatch', 'better_feature_images', 10, 3);

function better_feature_images(WP_HTTP_Response $result, WP_REST_Server $server, WP_REST_Request $request) {
    if ( $request->get_route() === '/wp/v2/posts') {
        $data = $result->get_data();
        $data = array_map(function ($post) {
            $post['better_featured_image'] = [
                'source_url' => wp_get_attachment_image_url($post['featured_media'])
            ];
            return $post;
        }, $data);
        $result->set_data($data);
        return $result;
    }
    return $result;
}
